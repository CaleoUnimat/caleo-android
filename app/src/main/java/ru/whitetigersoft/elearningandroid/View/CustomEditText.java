package ru.whitetigersoft.elearningandroid.View;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import ru.whitetigersoft.elearningandroid.R;

/**
 * Created by Александр on 10.08.2017.
 */

public class CustomEditText extends android.support.v7.widget.AppCompatEditText {

    public CustomEditText(Context context) {
        super(context);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima_Nova_Regular.otf");
        this.setTypeface(face);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0);
        try {
            int fontNumber;
            fontNumber = a.getInteger(R.styleable.CustomEditText_fontCustomViewEditText, 10);
            Typeface face;
            try {
                switch (fontNumber) {
                    case 0: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Bold.otf");
                        setFace(face);
                        break;
                    }
                    case 1: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Semibold.otf");
                        setFace(face);
                        break;
                    }
                    case 2: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Extrabold.otf");
                        setFace(face);
                        break;
                    }
                    case 3: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Black.otf");
                        setFace(face);
                        break;
                    }
                    case 4: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Condensed Black.otf");
                        setFace(face);
                        break;
                    }
                    case 5: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Extra Condensed Black.otf");
                        setFace(face);
                        break;
                    }
                    case 6: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Condensed Light.otf");
                        setFace(face);
                        break;
                    }
                    case 7: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Extra Condensed Light.otf");
                        setFace(face);
                        break;
                    }
                    case 8: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Extra Condensed Bold.otf");
                        setFace(face);
                        break;
                    }
                    case 9: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Extra Condensed Regular.otf");
                        setFace(face);
                        break;
                    }
                    case 10: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima_Nova_Regular.otf");
                        setFace(face);
                        break;
                    }
                    case 11: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/MagistralC-Bold.otf");
                        setFace(face);
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } finally {
            a.recycle();
        }
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima_Nova_Regular.otf");
        this.setTypeface(face);
    }

    private void setFace(Typeface face) {
        this.setTypeface(face);
    }

}
