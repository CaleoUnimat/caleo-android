package ru.whitetigersoft.elearningandroid.View;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;

/**
 * Created by Александр on 10.08.2017.
 */

public class CustomButton extends android.support.v7.widget.AppCompatButton {
    public CustomButton(Context context) {
        super(context);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima_Nova_Regular.otf");
        setGravity(Gravity.CENTER);
        this.setTypeface(face);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima_Nova_Regular.otf");
        setAllCaps(false);
        setGravity(Gravity.CENTER);
        this.setTypeface(face);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima_Nova_Regular.otf");
        this.setTypeface(face);
    }

}
