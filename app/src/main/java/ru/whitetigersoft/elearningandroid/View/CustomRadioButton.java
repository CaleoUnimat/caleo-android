package ru.whitetigersoft.elearningandroid.View;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import ru.whitetigersoft.elearningandroid.R;

/**
 * Created by Александр on 10.08.2017.
 */

public class CustomRadioButton extends android.support.v7.widget.AppCompatRadioButton {

    private Context context;

    public CustomRadioButton(Context context) {
        super(context);
        this.context = context;
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima_Nova_Regular.otf");
        this.setTypeface(face);
        setTextColor(ContextCompat.getColor(context, R.color.color_dark_grey));
        setBackground(ContextCompat.getDrawable(context, R.drawable.checkbox_radio_button));
        setButtonDrawable(ContextCompat.getDrawable(context, android.R.color.transparent));
    }

    @Override
    public int getCompoundPaddingRight() {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 33, context.getResources().getDisplayMetrics());
    }

    @Override
    public int getCompoundPaddingLeft() {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 33, context.getResources().getDisplayMetrics());
    }

    @Override
    public int getPaddingLeft() {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 33, context.getResources().getDisplayMetrics());
    }

    @Override
    public int getPaddingRight() {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 33, context.getResources().getDisplayMetrics());
    }

    public CustomRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0);
        try {
            int fontNumber;
            fontNumber = a.getInteger(R.styleable.CustomTextView_fontCustomView, 10);
            Typeface face;
            try {
                switch (fontNumber) {
                    case 0: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Bold.otf");
                        setFace(face);
                        break;
                    }
                    case 1: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Semibold.otf");
                        setFace(face);
                        break;
                    }
                    case 2: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Extrabold.otf");
                        setFace(face);
                        break;
                    }
                    case 3: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Black.otf");
                        setFace(face);
                        break;
                    }
                    case 4: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Condensed Black.otf");
                        setFace(face);
                        break;
                    }
                    case 5: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Extra Condensed Black.otf");
                        setFace(face);
                        break;
                    }
                    case 6: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Condensed Light.otf");
                        setFace(face);
                        break;
                    }
                    case 7: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Extra Condensed Light.otf");
                        setFace(face);
                        break;
                    }
                    case 8: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Extra Condensed Bold.otf");
                        setFace(face);
                        break;
                    }
                    case 9: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima Nova Extra Condensed Regular.otf");
                        setFace(face);
                        break;
                    }
                    case 10: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima_Nova_Regular.otf");
                        setFace(face);
                        break;
                    }
                    case 11: {
                        face = Typeface.createFromAsset(context.getAssets(), "fonts/MagistralC-Bold.otf");
                        setFace(face);
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } finally {
            a.recycle();
        }
    }

    public CustomRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Proxima_Nova_Regular.otf");
        this.setTypeface(face);
    }

    private void setFace(Typeface face) {
        this.setTypeface(face);
    }

}
