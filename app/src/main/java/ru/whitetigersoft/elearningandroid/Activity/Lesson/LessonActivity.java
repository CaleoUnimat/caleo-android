package ru.whitetigersoft.elearningandroid.Activity.Lesson;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubePlayer;
import com.thefinestartist.ytpa.enums.Orientation;
import com.thefinestartist.ytpa.utils.YouTubeUrlParser;

import java.util.ArrayList;
import java.util.List;

import ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.PdfView.PdfViewFragment;
import ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.Test.FragmentTest;
import ru.whitetigersoft.elearningandroid.Activity.Splash.SplashActivity;
import ru.whitetigersoft.elearningandroid.Activity.YouTube.YoutubePlayerActivity;
import ru.whitetigersoft.elearningandroid.Model.App.App;
import ru.whitetigersoft.elearningandroid.Model.BaseFragment.BaseFragment;
import ru.whitetigersoft.elearningandroid.Model.Helpers.ListHelper;
import ru.whitetigersoft.elearningandroid.Model.Helpers.StringHelper;
import ru.whitetigersoft.elearningandroid.Model.Models.Lesson;
import ru.whitetigersoft.elearningandroid.R;
import ru.whitetigersoft.elearningandroid.View.CustomTextView;

/**
 * Created by Александр on 24.08.2017.
 */
// TODO: 28.08.17 унаследовать от BaseActivity
// TODO: 07.09.2017 я здесь не зря наследуюсь от AppCompactActivity, такая логика проекта
public class LessonActivity extends AppCompatActivity {

    private static final int YOUTUBE_ACTIVITY_RESULT = 1;
    private static final String IS_REFRESH_LESSON = "isRefreshLesson";
    private static final String BUNDLE_PDF_KEY = "bundlePdfKey";
    private static final String BUNDLE_LESSON = "bundleLesson";
    private static final String BUNDLE_COURSE_ID_KEY = "bundleCourseIdKey";
    private static final String BUNDLE_RESULT_LESSON = "bundleResultLessonKey";
    private int courseId;
    private Lesson lesson;
    private ArrayList<Integer> disablePositions;
    private CustomTextView titleView;
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_lesson);
        super.onCreate(savedInstanceState);
        if (App.getInstance() != null) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            setCustomTitle();
            setupDrawer();
            if (!ListHelper.isEmpty(App.getInstance().getCourseManager().getDisablePositions())) {
                disablePositions = App.getInstance().getCourseManager().getDisablePositions();
            }
            this.lesson = getIntent().getParcelableExtra(BUNDLE_LESSON);
            this.courseId = getIntent().getIntExtra(BUNDLE_COURSE_ID_KEY, 0);
            startLesson();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.toast_message_error_on_load), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    private void setupDrawer() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitFromLesson();
            }
        });
    }

    private void setCustomTitle() {
        titleView = new CustomTextView(this);
        Typeface face = Typeface.createFromAsset(this.getAssets(), "fonts/MagistralC-Bold.otf");
        titleView.setTypeface(face);
        titleView.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        toolbar.addView(titleView);
    }

    @Override
    public void setTitle(CharSequence title) {
        try {
            super.setTitle("");
            titleView.setText(title);
        } catch (Exception e) {
            super.setTitle(title);
        }
    }

    private void startLesson() {
        if (lesson != null) {
            if (!ListHelper.isEmpty(disablePositions)) {
                disablePositions.remove(0);
            }
            setTitle(lesson.getTitle());
            switch (lesson.getType()) {
                case Lesson.TYPE_VIDEO:
                    startYouTube(lesson);
                    break;
                case Lesson.TYPE_TEST:
                    startTestFragment(lesson);
                    break;
                case Lesson.TYPE_TEXT:
                    startPdfView(lesson);
                    break;
            }
        } else {
            Snackbar.make(findViewById(R.id.container_lesson), getString(R.string.snack_bar_message_error_course_list), Snackbar.LENGTH_LONG).show();
            exitFromLesson();
        }
    }

    protected void  exitFromLesson() {
        Intent intent = new Intent();
        intent.putExtra(IS_REFRESH_LESSON, true);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void startNextLesson() {
        List<Lesson> lessonList = App.getInstance().getCourseManager().getLessonList();
        if (App.getInstance().getCourseManager().getCurrentLesson() != null) {
            lesson = App.getInstance().getCourseManager().getCurrentLesson();
        }
        int index = App.getInstance().getCourseManager().findIndexByLesson(lessonList, lesson);
        if (index + 1 < lessonList.size()) {
            Lesson nextLesson = App.getInstance().getCourseManager().getLessonList().get(index + 1);
            lesson = nextLesson;
            App.getInstance().getCourseManager().setCurrentLesson(nextLesson);
            if (!ListHelper.isEmpty(App.getInstance().getCourseManager().getDisablePositions())) {
                App.getInstance().getCourseManager().removePosition(0);
            }
            if (nextLesson.getType() == Lesson.TYPE_TEXT) {
                startPdfView(nextLesson);
            } else if (nextLesson.getType() == Lesson.TYPE_TEST) {
                startTestFragment(nextLesson);
            } else if (nextLesson.getType() == Lesson.TYPE_VIDEO) {
                startYouTube(nextLesson);
            }
        } else {
            exitFromLesson();
        }
    }

    private void startYouTube(Lesson lesson) {
        Intent intent = new Intent(this, YoutubePlayerActivity.class);
        if (!StringHelper.isEmpty(lesson.getYouTubeUrl())) {
            String videoId = YouTubeUrlParser.getVideoId(lesson.getYouTubeUrl());
            if (videoId != null) {
                intent.putExtra(YoutubePlayerActivity.EXTRA_VIDEO_ID, videoId)
                        .putExtra(YoutubePlayerActivity.EXTRA_LESSON, lesson)
                        .putExtra(YoutubePlayerActivity.EXTRA_COURSE_ID, courseId)
                        .putExtra(YoutubePlayerActivity.EXTRA_PLAYER_STYLE, YouTubePlayer.PlayerStyle.DEFAULT)
                        .putExtra(YoutubePlayerActivity.EXTRA_ORIENTATION, Orientation.AUTO)
                        .putExtra(YoutubePlayerActivity.EXTRA_SHOW_AUDIO_UI, true)
                        .putExtra(YoutubePlayerActivity.EXTRA_HANDLE_ERROR, true)
                        .putExtra(YoutubePlayerActivity.EXTRA_SEEK_TO, 0);
                startActivityForResult(intent, YOUTUBE_ACTIVITY_RESULT);
            } else {
                Snackbar.make(findViewById(R.id.container_lesson), getString(R.string.snack_bar_message_error_youtube), Snackbar.LENGTH_LONG).show();
            }
        } else {
            Snackbar.make(findViewById(R.id.container_lesson), getString(R.string.snack_bar_message_error_youtube), Snackbar.LENGTH_LONG).show();
        }
    }

    private void startTestFragment(Lesson lesson) {
        FragmentTest fragmentTest = new FragmentTest();
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_COURSE_ID_KEY, courseId);
        bundle.putParcelable(BUNDLE_RESULT_LESSON, lesson);
        fragmentTest.setArguments(bundle);
        replaceFragment(fragmentTest);
    }

    private void startPdfView(Lesson lesson) {
        PdfViewFragment pdfViewFragment = new PdfViewFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_COURSE_ID_KEY, courseId);
        bundle.putParcelable(BUNDLE_PDF_KEY, lesson);
        pdfViewFragment.setArguments(bundle);
        replaceFragment(pdfViewFragment);
    }

    protected void replaceFragment(BaseFragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.replace(R.id.container_lesson, fragment);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        exitFromLesson();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (data != null && data.hasExtra(YoutubePlayerActivity.NEXT_LESSON_FROM_YOUTUBE)) {
                startNextLesson();
            } else {
                exitFromLesson();
            }
        }
    }
}
