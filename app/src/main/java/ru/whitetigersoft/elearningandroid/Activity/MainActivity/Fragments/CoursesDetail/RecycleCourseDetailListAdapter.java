package ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.CoursesDetail;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.CoursesDetail.ViewHoldersCourseDetail.ViewHolderDetailCourse;
import ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.CoursesDetail.ViewHoldersCourseDetail.ViewHolderLesson;
import ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.CoursesDetail.ViewHoldersCourseDetail.ViewHolderLessonEmpty;
import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.CoursesApiListener;
import ru.whitetigersoft.elearningandroid.Model.App.App;
import ru.whitetigersoft.elearningandroid.Model.CustomListeners.OnEnableRecycleViewListener;
import ru.whitetigersoft.elearningandroid.Model.Helpers.StringHelper;
import ru.whitetigersoft.elearningandroid.Model.Models.Course;
import ru.whitetigersoft.elearningandroid.Model.Models.Lesson;
import ru.whitetigersoft.elearningandroid.R;

/**
 * Created by Александр on 19.07.2017.
 */

public class RecycleCourseDetailListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_VIEW_TYPE_HEAD = 1;
    private static final int ITEM_VIEW_TYPE_LESSON = 2;
    private static final int ITEM_VIEW_TYPE_EMPTY_LESSON = 3;
    private Context context;
    private List<Object> objList;
    private OnEnableRecycleViewListener onEnableRecycleViewListener;
    private LinearLayout linearLayoutProgressBar;

    public RecycleCourseDetailListAdapter(LinearLayout linearLayoutProgressBar, Context context, OnEnableRecycleViewListener onEnableRecycleViewListener) {
        this.context = context;
        this.objList = new ArrayList<>();
        this.onEnableRecycleViewListener = onEnableRecycleViewListener;
        this.linearLayoutProgressBar = linearLayoutProgressBar;
    }

    public void addToObjectList(List<Object> objectList) {
        objList.subList(1, objList.size()).clear();
        objList.addAll(objectList);
        notifyDataSetChanged();
    }

    public void addToObjectListLessons(List<Lesson> lessonList) {
        objList.subList(1, objList.size()).clear();
        objList.addAll(lessonList);
        notifyDataSetChanged();
    }

    public void addEmptyValue(String text) {
        this.objList.add(text);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM_VIEW_TYPE_HEAD;
        } else if (objList.get(1).getClass() == String.class) {
            return ITEM_VIEW_TYPE_EMPTY_LESSON;
        } else {
            return ITEM_VIEW_TYPE_LESSON;
        }
    }

    public Object getItem(int position) {
        return objList.get(position);
    }

    public List<Object> getObjectList() {
        return objList;
    }

    public void setObjectList(List<Object> objectList) {
        this.objList = objectList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_VIEW_TYPE_HEAD) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_fragment_detail_course, parent, false);
            return new ViewHolderDetailCourse(itemView);
        } else if (viewType == ITEM_VIEW_TYPE_LESSON) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_fragment_lesson, parent, false);
            return new ViewHolderLesson(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_fragment_empty_lesson, parent, false);
            return new ViewHolderLessonEmpty(itemView);
        }
    }

    private void setMaterialType(ViewHolderLesson holder, Lesson lesson) {
        switch (lesson.getType()) {
            case Lesson.TYPE_VIDEO:
                holder.getImageViewTypeMaterial().setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_lesson_video));
                break;
            case Lesson.TYPE_TEXT:
                holder.getImageViewTypeMaterial().setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_lesson_text));
                break;
            case Lesson.TYPE_TEST:
                holder.getImageViewTypeMaterial().setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_lesson_test));
                break;
        }
    }

    private void setDisableColorLesson(ViewHolderLesson viewHolderLesson) {
        viewHolderLesson.getImageViewIsCompleted().setVisibility(View.GONE);
        viewHolderLesson.getTextViewStatusPassedMaterial().setText(context.getString(R.string.text_view_lesson_not_access));
        viewHolderLesson.getTextViewStatusPassedMaterial().setTextColor(ContextCompat.getColor(context, R.color.color_warm_grey));
        viewHolderLesson.getTextViewMaterialId().setTextColor(ContextCompat.getColor(context, R.color.color_cool_grey));
    }

    private void setColorLessonIsNotCompleted(ViewHolderLesson viewHolderLesson) {
        viewHolderLesson.getImageViewIsCompleted().setVisibility(View.GONE);
        viewHolderLesson.getTextViewStatusPassedMaterial().setText(context.getString(R.string.text_view_lesson_access));
        viewHolderLesson.getTextViewStatusPassedMaterial().setTextColor(ContextCompat.getColor(context, R.color.color_warm_grey));
        viewHolderLesson.getTextViewMaterialId().setTextColor(ContextCompat.getColor(context, R.color.color_faded_orange));
    }

    private void setColorLessonIsCompleted(ViewHolderLesson viewHolderLesson) {
        viewHolderLesson.getImageViewIsCompleted().setVisibility(View.VISIBLE);
        viewHolderLesson.getTextViewStatusPassedMaterial().setText(context.getString(R.string.text_view_lesson_is_completed));
        viewHolderLesson.getTextViewStatusPassedMaterial().setTextColor(ContextCompat.getColor(context, R.color.color_apple_green));
        viewHolderLesson.getTextViewMaterialId().setTextColor(ContextCompat.getColor(context, R.color.color_faded_orange));
    }

    private void setMarginForFirstPosition(ViewHolderLesson viewHolderLesson) {
        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80, context.getResources().getDisplayMetrics());
        int marginLeftRight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, context.getResources().getDisplayMetrics());
        int marginTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, context.getResources().getDisplayMetrics());
        int marginBottom = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, context.getResources().getDisplayMetrics());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height);
        layoutParams.setMargins(marginLeftRight, marginTop, marginLeftRight, marginBottom);
        viewHolderLesson.getLinearLayoutLessonMain().setLayoutParams(layoutParams);
    }

    private void setLessonConfig(Lesson lesson, ViewHolderLesson viewHolderLesson, int position) {
        Course course = (Course) objList.get(0);
        int index = App.getInstance().getCourseManager().findIndexByLesson(lesson);
        setMaterialType(viewHolderLesson, lesson);
        viewHolderLesson.getTextViewNameMaterial().setText(lesson.getTitle());
        Lesson previousLesson = null;
        if (index >= 1) {
            previousLesson = App.getInstance().getCourseManager().getLessonList().get(index - 1);
        }
        if (position >= 1 && position < 10) {
            viewHolderLesson.getTextViewMaterialId().setText(String.valueOf("0" + position));
        } else {
            viewHolderLesson.getTextViewMaterialId().setText(String.valueOf(position));
        }
        if (position == 1) {
            setMarginForFirstPosition(viewHolderLesson);
        }
        if (previousLesson != null && previousLesson.isCompleted() == null) {
            onEnableRecycleViewListener.onDisable(position);
            setDisableColorLesson(viewHolderLesson);
        } else if (previousLesson != null && course.isStarted() && !previousLesson.isCompleted()) {
            onEnableRecycleViewListener.onDisable(position);
            setDisableColorLesson(viewHolderLesson);
        } else {
            if (lesson.isCompleted() == null && !course.isStarted() || (lesson.isCompleted() != null && !lesson.isCompleted() && !course.isStarted())) {
                setDisableColorLesson(viewHolderLesson);
            } else if (lesson.isCompleted() == null && course.isStarted() || (lesson.isCompleted() != null && !lesson.isCompleted() && course.isStarted())) {
                setColorLessonIsNotCompleted(viewHolderLesson);
            } else if (lesson.isCompleted() && course.isStarted()) {
                setColorLessonIsCompleted(viewHolderLesson);
            }
        }
    }

    private void setItemCourseInVisibilityProgress(ViewHolderDetailCourse viewHolderDetailCourse, Course course) {
        viewHolderDetailCourse.getButtonStartCourse().setVisibility(View.GONE);
        viewHolderDetailCourse.getProgressBarCourseDetail().setVisibility(View.VISIBLE);
        viewHolderDetailCourse.getTextViewProgressCourseDetail().setVisibility(View.VISIBLE);
        String textProgress;
        if ((int) course.getProgress() == 100) {
            textProgress = context.getString(R.string.text_view_lesson_is_completed);
        } else {
            textProgress = context.getString(R.string.constant_progress) + " " + (int) course.getProgress() + "/" + "100";
        }
        viewHolderDetailCourse.getTextViewProgressCourseDetail().setText(textProgress);
        viewHolderDetailCourse.getProgressBarCourseDetail().setProgress((int) course.getProgress());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(context.getString(R.string.constant_all_course_lessons));
        stringBuilder.append(" ");
        stringBuilder.append(String.valueOf(course.getPassedLessonCount()));
        stringBuilder.append(" ");
        stringBuilder.append(context.getString(R.string.constant_from));
        stringBuilder.append(" ");
        stringBuilder.append(String.valueOf(course.getLessonCount()));
        viewHolderDetailCourse.getTextViewCountOfCourses().setText(stringBuilder.toString());
    }

    private void setItemCourseInUnvisibilityProgress(ViewHolderDetailCourse viewHolderDetailCourse, Course course) {
        viewHolderDetailCourse.getProgressBarCourseDetail().setVisibility(View.GONE);
        viewHolderDetailCourse.getButtonStartCourse().setVisibility(View.VISIBLE);
        viewHolderDetailCourse.getTextViewProgressCourseDetail().setText(context.getString(R.string.text_view_lesson_is_not_started));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(context.getString(R.string.text_view_count_lessons));
        stringBuilder.append(" ");
        stringBuilder.append(String.valueOf(course.getLessonCount()));
        viewHolderDetailCourse.getTextViewCountOfCourses().setText(stringBuilder.toString());
        viewHolderDetailCourse.getButtonStartCourse().setOnClickListener(onButtonStartCourseClickListener());
    }

    private void setDetailCourseConfig(Course course, ViewHolderDetailCourse viewHolderDetailCourse) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.fitCenter().centerCrop();
        if (!StringHelper.isEmpty(course.getImageUrl())) {
            Glide.with(context).load(course.getImageUrl()).apply(requestOptions).into(viewHolderDetailCourse.getImageViewDetailCourse());
        } else {
            Glide.with(context).load(R.drawable.image1).into(viewHolderDetailCourse.getImageViewDetailCourse());
        }
        viewHolderDetailCourse.getTextViewNameCourseDetail().setText(course.getTitle());
        if (!StringHelper.isEmpty(course.getDescription())) {
            viewHolderDetailCourse.getTextViewDescriptionCourseDetail().setText(course.getDescription());
        } else {
            viewHolderDetailCourse.getLinearLayoutDescription().setVisibility(View.GONE);
        }
        if ((course.isStarted() && course.getLessonCount() != 0) || (course.getProgress() != 0 && course.getPassedLessonCount() != 0)) {
            setItemCourseInVisibilityProgress(viewHolderDetailCourse, course);
        } else {
            setItemCourseInUnvisibilityProgress(viewHolderDetailCourse, course);
        }
    }

    private void setEmptyLesson(String text, ViewHolderLessonEmpty viewHolderLessonEmpty) {
        viewHolderLessonEmpty.getTextViewEmpty().setText(text);
    }

    private View.OnClickListener onButtonStartCourseClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(context.getString(R.string.constant_title_start_course));
                builder.setCancelable(true);
                builder.setPositiveButton(
                        context.getString(R.string.constant_action_yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Button startButton = (Button) v;
                                sendStartCourse(startButton);
                            }
                        });

                builder.setNegativeButton(
                        context.getString(R.string.constant_action_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        };
    }

    private void sendStartCourse(final Button startButton) {
        Course course = (Course) objList.get(0);
        int courseId = course.getCourseId();
        linearLayoutProgressBar.setVisibility(View.VISIBLE);
        linearLayoutProgressBar.bringToFront();
        App.getInstance().getApi().getCoursesApi().startCourse(courseId, new CoursesApiListener() {
            @Override
            public void onSuccess() {
                linearLayoutProgressBar.setVisibility(View.GONE);
                startButton.setVisibility(View.GONE);
                onEnableRecycleViewListener.onEnable();
            }

            @Override
            public void onFailure() {
                linearLayoutProgressBar.setVisibility(View.GONE);
                onEnableRecycleViewListener.onFailure();
            }

            @Override
            public void onFailure(String errorString) {
                linearLayoutProgressBar.setVisibility(View.GONE);
                onEnableRecycleViewListener.onFailure(errorString);
            }
        });
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object object = objList.get(position);
        if (object.getClass() == Lesson.class) {
            Lesson lesson = (Lesson) object;
            ViewHolderLesson viewHolderLesson = (ViewHolderLesson) holder;
            setLessonConfig(lesson, viewHolderLesson, position);
        } else if (object.getClass() == Course.class) {
            Course course = (Course) object;
            ViewHolderDetailCourse viewHolderDetailCourse = (ViewHolderDetailCourse) holder;
            setDetailCourseConfig(course, viewHolderDetailCourse);
        } else if (object.getClass() == String.class) {
            String text = (String) object;
            ViewHolderLessonEmpty viewHolderLessonEmpty = (ViewHolderLessonEmpty) holder;
            setEmptyLesson(text, viewHolderLessonEmpty);
        }
    }

    @Override
    public int getItemCount() {
        return objList.size();
    }
}
