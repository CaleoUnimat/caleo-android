package ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.CoursesDetail.ViewHoldersCourseDetail;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import ru.whitetigersoft.elearningandroid.R;

/**
 * Created by Александр on 19.07.2017.
 */

public class ViewHolderDetailCourse extends RecyclerView.ViewHolder {


    private final TextView textViewCountOfCourses;
    private final ProgressBar progressBarCourseDetail;
    private final TextView textViewProgressCourseDetail;
    private final TextView textViewDescriptionCourseDetail;
    private final TextView textViewNameCourseDetail;
    private final ImageView imageViewDetailCourse;
    private final Button buttonStartCourse;
    private final LinearLayout linearLayoutDescription;

    public ViewHolderDetailCourse(View itemView) {
        super(itemView);
        this.textViewCountOfCourses = (TextView) itemView.findViewById(R.id.text_view_count_of_courses);
        this.progressBarCourseDetail = (ProgressBar) itemView.findViewById(R.id.progress_bar_course_detail);
        this.textViewProgressCourseDetail = (TextView) itemView.findViewById(R.id.text_view_progress_course_detail);
        this.textViewDescriptionCourseDetail = (TextView) itemView.findViewById(R.id.text_view_description_course_detail);
        this.textViewNameCourseDetail = (TextView) itemView.findViewById(R.id.text_view_name_course_detail);
        this.imageViewDetailCourse = (ImageView) itemView.findViewById(R.id.image_view_detail_course);
        this.buttonStartCourse = (Button) itemView.findViewById(R.id.button_start_course);
        this.linearLayoutDescription = (LinearLayout) itemView.findViewById(R.id.linear_layout_description);
    }

    public TextView getTextViewCountOfCourses() {
        return textViewCountOfCourses;
    }

    public ProgressBar getProgressBarCourseDetail() {
        return progressBarCourseDetail;
    }

    public TextView getTextViewProgressCourseDetail() {
        return textViewProgressCourseDetail;
    }

    public TextView getTextViewDescriptionCourseDetail() {
        return textViewDescriptionCourseDetail;
    }

    public TextView getTextViewNameCourseDetail() {
        return textViewNameCourseDetail;
    }

    public ImageView getImageViewDetailCourse() {
        return imageViewDetailCourse;
    }

    public Button getButtonStartCourse() {
        return buttonStartCourse;
    }

    public LinearLayout getLinearLayoutDescription() {
        return linearLayoutDescription;
    }
}
