package ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.Courses;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import ru.whitetigersoft.elearningandroid.Activity.Login.LoginActivity;
import ru.whitetigersoft.elearningandroid.Activity.MainActivity.MainActivity;
import ru.whitetigersoft.elearningandroid.Model.App.App;
import ru.whitetigersoft.elearningandroid.Model.BaseFragment.BaseFragment;
import ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.CoursesDetail.CoursesDetailFragment;
import ru.whitetigersoft.elearningandroid.Model.DataProviders.CourseDataProvider;
import ru.whitetigersoft.elearningandroid.Model.DataProviders.DataProviderListener.BaseDataProviderListener;
import ru.whitetigersoft.elearningandroid.Model.Helpers.ItemClickSupport;
import ru.whitetigersoft.elearningandroid.Model.Models.Course;
import ru.whitetigersoft.elearningandroid.R;

/**
 * Created by Александр on 19.07.2017.
 */

public class CoursesListFragment extends BaseFragment {

    private static final String BUNDLE_COURSE_ID_KEY = "bundleCourseIdKey";
    private static final String BUNDLE_COURSE_NAME_KEY = "bundleCourseTempNameKey";
    private Context context;
    private View rootView;
    private RecycleCourseListAdapter recycleCourseListAdapter;
    private CourseDataProvider courseDataProvider;
    private LinearLayout loadingPanel;


    @Override
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        context = getActivity();
        courseDataProvider = new CourseDataProvider();
        setHasOptionsMenu(true);
    }

    private void startDetailCourseFragment(int id) {
        CoursesDetailFragment coursesDetailFragment = new CoursesDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_COURSE_NAME_KEY, id);
        coursesDetailFragment.setArguments(bundle);
        replaceFragment(coursesDetailFragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        rootView = inflater.inflate(R.layout.fragemnt_courses, container, false);
        setupRecyclerView();
        reloadData();
        setTitle(getString(R.string.title_courses));
        return rootView;
    }

    private BaseDataProviderListener getCourseDataProvider() {
        return new BaseDataProviderListener() {
            @Override
            public void onItemsLoaded(List<?> items) {
                hideProgressBar();
                Bundle bundle = getArguments();
                if (bundle != null) {
                    int courseDetailId = bundle.getInt(BUNDLE_COURSE_NAME_KEY);
                    startDetailCourseFragment(courseDetailId);
                }
                recycleCourseListAdapter.setCourseList((List<Course>) items);
            }

            @Override
            public void onError() {
                onError(context.getString(R.string.snack_bar_message_error_course_list));
            }

            @Override
            public void onError(String errorString) {
                hideProgressBar();
                showSnackBarError(rootView, errorString, courseDataProvider);
            }
        };
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_exit, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_exit:
                App.getInstance().getAppUser().logOut();
                startLoginActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    private void startLoginActivity() {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        startActivity(intent);
    }

    private void reloadData() {
        showProgressBarWithLongerDuration(getString(R.string.progress_bat_text_course_list));
        courseDataProvider.reloadData();
    }

    @Override
    public void showProgressBarWithLongerDuration(String text) {
        loadingPanel.setVisibility(View.VISIBLE);
        loadingPanel.bringToFront();
    }

    @Override
    public void hideProgressBar() {
        loadingPanel.setVisibility(View.GONE);
    }

    private ItemClickSupport.OnItemClickListener createItemClickSupport() {
        return new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                CoursesDetailFragment coursesDetailFragment = new CoursesDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(BUNDLE_COURSE_ID_KEY, recycleCourseListAdapter.getItem(position).getCourseId());
                coursesDetailFragment.setArguments(bundle);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.addToBackStack(FRAGMENT_ADD_TO_BACKSTACK_COURSE_DETAIL);
                transaction.replace(R.id.container, coursesDetailFragment);
                transaction.commit();
            }
        };
    }

    private void setupRecyclerView() {
        RecyclerView recycleViewFragmentCourse = (RecyclerView) rootView.findViewById(R.id.recycle_view_fragment_course);
        recycleCourseListAdapter = new RecycleCourseListAdapter(context);
        this.loadingPanel = (LinearLayout) rootView.findViewById(R.id.loading_panel);
        courseDataProvider.setBaseDataProvider(getCourseDataProvider());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycleViewFragmentCourse.setLayoutManager(layoutManager);
        ItemClickSupport.addTo(recycleViewFragmentCourse).setOnItemClickListener(createItemClickSupport());
        recycleViewFragmentCourse.setAdapter(recycleCourseListAdapter);
    }
}
