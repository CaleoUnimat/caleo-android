package ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.Courses;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import ru.whitetigersoft.elearningandroid.Model.Helpers.StringHelper;
import ru.whitetigersoft.elearningandroid.Model.Models.Course;
import ru.whitetigersoft.elearningandroid.R;

/**
 * Created by Александр on 19.07.2017.
 */

public class RecycleCourseListAdapter extends RecyclerView.Adapter<ViewHolderCourse> {

    private Context context;
    private List<Course> courseList;

    public RecycleCourseListAdapter(Context context) {
        this.context = context;
        this.courseList = new ArrayList<>();
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolderCourse onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_fragment_courses, parent, false);
        return new ViewHolderCourse(itemView);
    }

    private void setVisibleProgressCourse(Course course, ViewHolderCourse holder) {
        String textProgress;
        if (course.getProgress() == 100) {
            textProgress =  context.getString(R.string.text_view_lesson_is_completed);
        } else {
            textProgress = context.getString(R.string.constant_progress) + " " + (int) course.getProgress() + "/" + "100";
        }
        holder.getTextViewProgressCourse().setText(textProgress);
        holder.getProgressBarCourse().setVisibility(View.VISIBLE);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, 0);
        holder.getTextViewProgressCourse().setLayoutParams(layoutParams);
        holder.getProgressBarCourse().setProgress((int) course.getProgress());
    }

    private void setUnvisibleProgressCourse(ViewHolderCourse holder) {
        holder.getTextViewProgressCourse().setText(context.getString(R.string.text_view_lesson_is_not_started));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int marginBottom = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, context.getResources().getDisplayMetrics());
        layoutParams.setMargins(0, 0, 0, marginBottom);
        holder.getTextViewProgressCourse().setLayoutParams(layoutParams);
        holder.getProgressBarCourse().setVisibility(View.GONE);
    }

    @Override
    public void onBindViewHolder(ViewHolderCourse holder, int position) {
        Course course = courseList.get(position);
        holder.getTextViewNameCourse().setText(course.getTitle());
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.fitCenter().centerCrop();
        if (!StringHelper.isEmpty(course.getImageUrl())) {
            Glide.with(context).load(course.getImageUrl()).apply(requestOptions).into(holder.getImageViewCourses());
        } else {
            Glide.with(context).load(R.drawable.image1).apply(requestOptions).into(holder.getImageViewCourses());
        }
        if (!course.isStarted()) {
            setUnvisibleProgressCourse(holder);
        } else if (course.getProgress() >= 0 && course.isStarted()) {
            setVisibleProgressCourse(course, holder);
        }
    }

    @Override
    public int getItemCount() {
        return courseList.size();
    }

    public Course getItem(int position) {
        return courseList.get(position);
    }
}
