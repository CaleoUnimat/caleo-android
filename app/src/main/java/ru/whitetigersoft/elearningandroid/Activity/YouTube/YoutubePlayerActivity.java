package ru.whitetigersoft.elearningandroid.Activity.YouTube;

/**
 * Created by valerijsamsonov on 10.11.16.
 */

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout.LayoutParams;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.ErrorReason;
import com.google.android.youtube.player.YouTubePlayerView;
import com.thefinestartist.ytpa.enums.Orientation;
import com.thefinestartist.ytpa.utils.AudioUtil;
import com.thefinestartist.ytpa.utils.StatusBarUtil;
import com.thefinestartist.ytpa.utils.YouTubeApp;
import com.thefinestartist.ytpa.utils.YouTubeUrlParser;

import java.util.List;

import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.LessonApiListener;
import ru.whitetigersoft.elearningandroid.Model.App.App;
import ru.whitetigersoft.elearningandroid.Model.BaseFragment.BaseFragment;
import ru.whitetigersoft.elearningandroid.Model.Helpers.ListHelper;
import ru.whitetigersoft.elearningandroid.Model.Helpers.StringHelper;
import ru.whitetigersoft.elearningandroid.Model.Models.Lesson;
import ru.whitetigersoft.elearningandroid.R;
import ru.whitetigersoft.elearningandroid.View.CustomButton;

public class YoutubePlayerActivity extends YouTubeBaseActivity implements
        YouTubePlayer.OnInitializedListener,
        YouTubePlayer.OnFullscreenListener,
        YouTubePlayer.PlayerStateChangeListener {

    public final static String NEXT_LESSON_FROM_YOUTUBE = "isNextLesson";
    public static final String EXTRA_VIDEO_ID = "video_id";
    public static final String EXTRA_PLAYER_STYLE = "player_style";
    public static final String EXTRA_ORIENTATION = "orientation";
    public static final String EXTRA_SHOW_AUDIO_UI = "show_audio_ui";
    public static final String EXTRA_HANDLE_ERROR = "handle_error";
    public static final String EXTRA_ANIM_ENTER = "anim_enter";
    public static final String EXTRA_ANIM_EXIT = "anim_exit";
    public static final String EXTRA_SEEK_TO = "seek_to";
    public static final String EXTRA_FULL_SCREEN = "full_screen";
    public static final String EXTRA_LESSON = "lesson";
    public static final String EXTRA_COURSE_ID = "bundleCourseIdKey";
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private static final int YOUTUBE_ACTIVITY_RESULT = 1;
    private static final String META_DATA_NAME = "com.thefinestartist.ytpa.YouTubePlayerActivity.ApiKey";

    @SuppressLint("InlinedApi")
    private static final int PORTRAIT_ORIENTATION = Build.VERSION.SDK_INT < 9
            ? ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            : ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT;
    @SuppressLint("InlinedApi")
    private static final int LANDSCAPE_ORIENTATION = Build.VERSION.SDK_INT < 9
            ? ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            : ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE;

    String TAG = "fffppp";
    private String googleApiKey;
    private String videoId;
    private YouTubePlayer.PlayerStyle playerStyle;
    private Orientation orientation;
    private Lesson lesson;
    private boolean showAudioUi;
    private boolean handleError;
    private boolean isFullScreen;
    private int animEnter;
    private int animExit;
    private int startTime;
    private int courseId;
    private YouTubePlayerView playerView;
    private YouTubePlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        playerView = new YouTubePlayerView(this);
        playerView.initialize(googleApiKey, this);
        setTitle(getString(R.string.constant_title_lesson) + ": " + lesson.getTitle());
        addContentView(playerView, new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));

        playerView.setBackgroundResource(android.R.color.black);
        StatusBarUtil.hide(this);
    }

    private void initialize() {
        try {
            ApplicationInfo ai = getPackageManager().getApplicationInfo(getPackageName(),
                    PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            googleApiKey = bundle.getString(META_DATA_NAME);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (googleApiKey == null)
            throw new NullPointerException("Google API key must not be null. Set your api key as meta data in AndroidManifest.xml file.");

        videoId = getIntent().getStringExtra(EXTRA_VIDEO_ID);
        if (videoId == null)
            throw new NullPointerException("Video ID must not be null");

        playerStyle = (YouTubePlayer.PlayerStyle) getIntent().getSerializableExtra(EXTRA_PLAYER_STYLE);
        if (playerStyle == null)
            playerStyle = YouTubePlayer.PlayerStyle.DEFAULT;

        orientation = (Orientation) getIntent().getSerializableExtra(EXTRA_ORIENTATION);
        if (orientation == null)
            orientation = Orientation.AUTO;

        lesson = getIntent().getParcelableExtra(EXTRA_LESSON);
        if (lesson == null)
            throw new NullPointerException("Not send object Lesson");

        courseId = getIntent().getIntExtra(EXTRA_COURSE_ID, 0);
        isFullScreen = getIntent().getBooleanExtra(EXTRA_FULL_SCREEN, false);
        showAudioUi = getIntent().getBooleanExtra(EXTRA_SHOW_AUDIO_UI, true);
        handleError = getIntent().getBooleanExtra(EXTRA_HANDLE_ERROR, true);
        animEnter = getIntent().getIntExtra(EXTRA_ANIM_ENTER, 0);
        animExit = getIntent().getIntExtra(EXTRA_ANIM_EXIT, 0);
        startTime = getIntent().getIntExtra(EXTRA_SEEK_TO, 0);
    }

    private void startNextLesson() {
        List<Lesson> lessonList = App.getInstance().getCourseManager().getLessonList();
        int index = App.getInstance().getCourseManager().findIndexByLesson(lessonList, lesson);
        if (index + 1 < lessonList.size()) {
            Lesson nextLesson = App.getInstance().getCourseManager().getLessonList().get(index + 1);
            App.getInstance().getCourseManager().setCurrentLesson(lesson);
            if (!ListHelper.isEmpty(App.getInstance().getCourseManager().getDisablePositions())) {
                App.getInstance().getCourseManager().removePosition(0);
            }
            if (nextLesson.getType() == Lesson.TYPE_TEXT && lesson != null || nextLesson.getType() == Lesson.TYPE_TEST && lesson != null) {
                startOtherFragment();
            } else if (nextLesson.getType() == Lesson.TYPE_VIDEO && lesson != null) {
                startYouTube(nextLesson, courseId);
            }
        } else {
            setResult(RESULT_OK);
            finish();
        }
    }

    protected void startOtherFragment() {
        Intent intent = new Intent();
        intent.putExtra(NEXT_LESSON_FROM_YOUTUBE, true);
        setResult(RESULT_OK, intent);
        finish();
    }

    protected void startYouTube(Lesson lesson, int courseId) {
        Intent intent = new Intent(this, YoutubePlayerActivity.class);
        if (!StringHelper.isEmpty(lesson.getYouTubeUrl())) {
            String videoId = YouTubeUrlParser.getVideoId(lesson.getYouTubeUrl());
            if (videoId != null) {
                intent.putExtra(YoutubePlayerActivity.EXTRA_VIDEO_ID, videoId)
                        .putExtra(YoutubePlayerActivity.EXTRA_LESSON, lesson)
                        .putExtra(YoutubePlayerActivity.EXTRA_COURSE_ID, courseId)
                        .putExtra(YoutubePlayerActivity.EXTRA_PLAYER_STYLE, YouTubePlayer.PlayerStyle.DEFAULT)
                        .putExtra(YoutubePlayerActivity.EXTRA_ORIENTATION, Orientation.AUTO)
                        .putExtra(YoutubePlayerActivity.EXTRA_SHOW_AUDIO_UI, true)
                        .putExtra(YoutubePlayerActivity.EXTRA_HANDLE_ERROR, true)
                        .putExtra(YoutubePlayerActivity.EXTRA_SEEK_TO, 0);
                startActivityForResult(intent, YOUTUBE_ACTIVITY_RESULT);
            } else {
                Snackbar.make(findViewById(R.id.container_lesson), getString(R.string.snack_bar_message_error_youtube), Snackbar.LENGTH_LONG).show();
                setResult(RESULT_OK);
                finish();
            }
        } else {
            Snackbar.make(findViewById(R.id.container_lesson), getString(R.string.snack_bar_message_error_youtube), Snackbar.LENGTH_LONG).show();
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                        YouTubePlayer player,
                                        boolean wasRestored) {
        this.player = player;
        this.player.seekToMillis(startTime);

        player.setOnFullscreenListener(this);
        player.setPlayerStateChangeListener(this);
        player.setFullscreen(isFullScreen);
        switch (orientation) {
            case AUTO:
                player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION
                        | YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI
                        | YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE
                        | YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
                break;
            case AUTO_START_WITH_LANDSCAPE:
                player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION
                        | YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI
                        | YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE
                        | YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
                break;
            case ONLY_LANDSCAPE:
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI
                        | YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
                break;
            case ONLY_PORTRAIT:
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI
                        | YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
                break;
        }

        switch (playerStyle) {
            case CHROMELESS:
                player.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
                break;
            case MINIMAL:
                player.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
                break;
            case DEFAULT:
            default:
                player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                break;
        }

        if (!wasRestored) {
            if (startTime <= 0) {
                player.loadVideo(videoId);
                Log.d(TAG, "onInitializationSuccess: time <= 0");
            } else {
                Log.d(TAG, "onInitializationSuccess: time > 0");
                player.loadVideo(videoId, startTime * 1000);
            }

        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    "There was an error initializing the YouTubePlayer (%1$s)",
                    errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            playerView.initialize(googleApiKey, this);
        }
        if (requestCode == YOUTUBE_ACTIVITY_RESULT) {
            Intent intent = new Intent();
            if (data != null) {
                if (data.hasExtra(NEXT_LESSON_FROM_YOUTUBE)) {
                    intent.putExtra(NEXT_LESSON_FROM_YOUTUBE, true);
                }
            }
            if (intent.hasExtra(NEXT_LESSON_FROM_YOUTUBE)) {
                setResult(RESULT_OK, intent);
            } else {
                setResult(RESULT_OK);
            }
            finish();
        }
    }

    // YouTubePlayer.OnFullscreenListener
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        switch (orientation) {
            case AUTO:
            case AUTO_START_WITH_LANDSCAPE:
                if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    if (player != null)
                        player.setFullscreen(true);
                } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
                    if (player != null)
                        player.setFullscreen(false);
                }
                break;
            case ONLY_LANDSCAPE:
            case ONLY_PORTRAIT:
                break;
        }
    }

    @Override
    public void onFullscreen(boolean fullScreen) {
        switch (orientation) {
            case AUTO:
            case AUTO_START_WITH_LANDSCAPE:
                if (fullScreen)
                    setRequestedOrientation(LANDSCAPE_ORIENTATION);
                else
                    setRequestedOrientation(PORTRAIT_ORIENTATION);
                break;
            case ONLY_LANDSCAPE:
            case ONLY_PORTRAIT:
                break;
        }
    }

    // YouTubePlayer.PlayerStateChangeListener
    @Override
    public void onError(ErrorReason reason) {
        Log.e("onError", "onError : " + reason.name());
        if (handleError && ErrorReason.NOT_PLAYABLE.equals(reason))
            YouTubeApp.startVideo(this, videoId);
    }

    @Override
    public void onAdStarted() {
    }

    @Override
    public void onLoaded(String videoId) {
    }

    @Override
    public void onLoading() {
    }

    @Override
    public void onVideoEnded() {
        passVideoLesson();
    }

    public void showToastError(String text) {
        if (playerView != null) {
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
        }
    }

    private void passVideoLesson() {
        App.getInstance().getApi().getLessonApi().passVideoLesson(lesson.getLessonId(), 100, new LessonApiListener() {
            @Override
            public void onSuccess() {
                final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(YoutubePlayerActivity.this, R.style.AppTheme_NoActionBar));
                builder.setCancelable(true);
                String title;
                final int index = App.getInstance().getCourseManager().findIndexByLesson(lesson);
                final boolean isNextLesson;
                if (index + 1 < App.getInstance().getCourseManager().getLessonList().size()) {
                    title = getString(R.string.button_title_next_lesson);
                    isNextLesson = true;
                } else {
                    title = getString(R.string.button_title_finish_lesson);
                    isNextLesson = false;
                }
                LayoutInflater inflater = getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.alert_dialog_custom_view, null);
                CustomButton nextLessonButton = (CustomButton) dialogLayout.findViewById(R.id.alert_dialog_next_lesson);
                nextLessonButton.setText(title);
                nextLessonButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isNextLesson) {
                            startNextLesson();
                        } else {
                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                });
                builder.setView(dialogLayout);
                final AlertDialog alertDialog = builder.create();
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                alertDialog.show();
            }

            @Override
            public void onFailure() {
                onFailure(getString(R.string.snack_bar_message_error_pass_test));
            }

            @Override
            public void onFailure(String errorString) {
                showToastError(errorString);
            }
        });
    }


    @Override
    public void onVideoStarted() {
        StatusBarUtil.hide(this);
    }

    // Audio Managing
    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            AudioUtil.adjustMusicVolume(getApplicationContext(), true, showAudioUi);
            StatusBarUtil.hide(this);
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            AudioUtil.adjustMusicVolume(getApplicationContext(), false, showAudioUi);
            StatusBarUtil.hide(this);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(BaseFragment.IS_REFRESH_LESSON, true);
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }
}
