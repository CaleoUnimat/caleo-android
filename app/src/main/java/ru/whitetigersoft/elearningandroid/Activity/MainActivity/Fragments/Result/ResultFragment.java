package ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.Result;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.ResultApiListener;
import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.TestApiListener;
import ru.whitetigersoft.elearningandroid.Model.App.App;
import ru.whitetigersoft.elearningandroid.Model.BaseFragment.BaseFragment;
import ru.whitetigersoft.elearningandroid.Model.DataProviders.BaseDataProvider;
import ru.whitetigersoft.elearningandroid.Model.Helpers.DateHelper;
import ru.whitetigersoft.elearningandroid.Model.Helpers.ListHelper;
import ru.whitetigersoft.elearningandroid.Model.Models.Lesson;
import ru.whitetigersoft.elearningandroid.Model.Models.Result;
import ru.whitetigersoft.elearningandroid.R;

/**
 * Created by Александр on 24.07.2017.
 */

public class ResultFragment extends BaseFragment {

    private static final String BUNDLE_COURSE_ID_KEY = "bundleCourseIdKey";
    private static final String BUNDLE_RESULT_LESSON = "bundleResultLessonKey";
    private static final String BUNDLE_ANSWERS_PASS_KEY = "bundleAnswersPassKey";
    private Context context;
    private TextView textViewCountOfQuestions;
    private TextView textViewNumberOfCorrectAnswers;
    private TextView textViewPercentOfCorrectAnswers;
    private TextView textViewNumberOfNotCorrectAnswers;
    private TextView textViewPercentOfNotCorrectAnswers;
    private TextView textViewTestPassedMark;
    private Button buttonFinishTest;
    private Lesson lesson;
    private int courseId;
    private String answers;
    private LinearLayout linearLayoutMain;

    @Override
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        this.context = getActivity();
        Bundle bundle = getArguments();
        if (bundle != null) {
            this.courseId = bundle.getInt(BUNDLE_COURSE_ID_KEY);
            this.lesson = bundle.getParcelable(BUNDLE_RESULT_LESSON);
            this.answers = bundle.getString(BUNDLE_ANSWERS_PASS_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_result, container, false);
        String title = getString(R.string.title_test) + ": " + this.lesson.getTitle();
        setTitle(title);
        prepareViews(rootView);
        loadData();
        return rootView;
    }

    private void loadData() {
        showProgressBarWithLongerDuration(getString(R.string.progress_bar_text_result));
        App.getInstance().getApi().getTestApi().passTest(lesson.getLessonId(), answers, new TestApiListener() {
            @Override
            public void onSuccess() {
                getResult();
            }

            @Override
            public void onFailure() {
                onFailure(getString(R.string.snack_bar_message_error_pass_test));
            }

            @Override
            public void onFailure(String errorString) {
                showSnackBarError(getView(), errorString, null);
            }
        });
    }

    private void getResult() {
        App.getInstance().getApi().getResultApi().getResult(lesson.getLessonId(), new ResultApiListener() {
            @Override
            public void onResultLoaded(Result result) {
                buttonFinishTest.setVisibility(View.VISIBLE);
                linearLayoutMain.setVisibility(View.VISIBLE);
                hideProgressBar();
                fillData(result);
            }

            @Override
            public void onFailure() {
                onFailure(getString(R.string.snack_bar_message_error_result));
            }

            @Override
            public void onFailure(String errorString) {
                showSnackBarError(getView(), errorString, null);
            }
        });
    }

    private void startNextLesson() {
        List<Lesson> lessonList = App.getInstance().getCourseManager().getLessonList();
        int index = App.getInstance().getCourseManager().findIndexByLesson(lessonList, lesson);
        if (index + 1 < lessonList.size()) {
            Lesson nextLesson = App.getInstance().getCourseManager().getLessonList().get(index + 1);
            if (!ListHelper.isEmpty(App.getInstance().getCourseManager().getDisablePositions())) {
                App.getInstance().getCourseManager().removePosition(0);
            }
            if (nextLesson.getType() == Lesson.TYPE_TEXT && lesson != null) {
                App.getInstance().getCourseManager().setCurrentLesson(lesson);
                startPdfView(nextLesson);
            } else if (nextLesson.getType() == Lesson.TYPE_TEST && lesson != null) {
                App.getInstance().getCourseManager().setCurrentLesson(lesson);
                startTestFragment(nextLesson, courseId);
            } else if (nextLesson.getType() == Lesson.TYPE_VIDEO && lesson != null) {
                App.getInstance().getCourseManager().setCurrentLesson(lesson);
                startYouTube(nextLesson, courseId);
            }
        } else {
            exitFromLesson();
        }
    }

    private void showSnackBarForBlockedTest(Integer remainingTime) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime() + (remainingTime * 1000));
        String stringDate = DateHelper.getTimeStringFromCalendar(calendar);
        String text = context.getString(R.string.constant_remaining_time_message) + " " + stringDate;
        if (getView() != null) {
            Snackbar.make(getView(), text, Snackbar.LENGTH_SHORT).show();
        }
    }

    private View.OnClickListener onButtonRepeatClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.getInstance().getApi().getTestApi().checkPassingTest(lesson.getTestLessonId(), new TestApiListener() {
                    @Override
                    public void onCheckTestPassing(Integer remainingTime) {
                        if (remainingTime > 0) {
                            showSnackBarForBlockedTest(remainingTime);
                        } else {
                            startTestFragment(lesson, courseId);
                        }
                    }

                    @Override
                    public void onFailure() {
                        onFailure(context.getString(R.string.snack_bar_message_error_pass_test));
                    }

                    @Override
                    public void onFailure(String errorString) {
                        showSnackBarError(getView(), errorString, null);
                    }
                });
            }
        };
    }

    @Override
    public void showSnackBarError(View view, String text, BaseDataProvider baseDataProvider) {
        if (getContext() != null && view != null) {
            Snackbar snackbar = getSnackbar(view, text, SNACK_BAR_ERROR_DURATION)
                    .setAction(getString(R.string.snackbar_button_reload), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (getContext() != null) {
                                loadData();
                            }
                        }
                    });
            snackbar.show();
        }
    }


    private View.OnClickListener onButtonFinishClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNextLesson();
            }
        };
    }

    private void fillData(Result result) {
        textViewPercentOfNotCorrectAnswers.setText(String.valueOf((int) result.getWrongAnswersProgress() + "%"));
        textViewNumberOfNotCorrectAnswers.setText(String.valueOf(result.getWrongAnswers()));
        textViewPercentOfCorrectAnswers.setText(String.valueOf((int) result.getCorrectAnswersProgress() + "%"));
        textViewNumberOfCorrectAnswers.setText(String.valueOf(result.getCorrectAnswers()));
        textViewCountOfQuestions.setText(String.valueOf(result.getQuestionCount()));
        buttonFinishTest.setTextColor(ContextCompat.getColor(context, android.R.color.white));
        if (result.isCompleted()) {
            textViewTestPassedMark.setTextColor(ContextCompat.getColor(context, R.color.color_apple_green));
            textViewTestPassedMark.setText(getString(R.string.text_view_test_result_test_completed));
            buttonFinishTest.setBackground(ContextCompat.getDrawable(context, R.drawable.button_background_circle_fill_green));
            buttonFinishTest.setText(getString(R.string.button_title_finish_test));
            buttonFinishTest.setOnClickListener(onButtonFinishClickListener());
        } else {
            textViewTestPassedMark.setText(getString(R.string.text_view_test_result_test_non_completed));
            textViewTestPassedMark.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            buttonFinishTest.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_radio_button_checked));
            buttonFinishTest.setText(getString(R.string.button_title_repeat_test));
            buttonFinishTest.setOnClickListener(onButtonRepeatClickListener());
        }
    }

    private void prepareViews(View rootView) {
        this.linearLayoutMain = (LinearLayout) rootView.findViewById(R.id.linear_layout_result_main);
        this.textViewPercentOfNotCorrectAnswers = (TextView) rootView.findViewById(R.id.text_view_percent_of_not_correct_answers_value);
        this.textViewNumberOfNotCorrectAnswers = (TextView) rootView.findViewById(R.id.text_view_number_of_not_correct_answers_value);
        this.textViewPercentOfCorrectAnswers = (TextView) rootView.findViewById(R.id.text_view_percent_of_correct_answers_value);
        this.textViewNumberOfCorrectAnswers = (TextView) rootView.findViewById(R.id.text_view_number_of_correct_answers_value);
        this.textViewTestPassedMark = (TextView) rootView.findViewById(R.id.text_view_test_passed_mark);
        this.textViewCountOfQuestions = (TextView) rootView.findViewById(R.id.text_view_count_of_questions_value);
        this.buttonFinishTest = (Button) rootView.findViewById(R.id.button_finish_test);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
