package ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.CoursesDetail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ru.whitetigersoft.elearningandroid.Activity.Lesson.LessonActivity;
import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.CoursesApiListener;
import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.LessonApiListener;
import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.TestApiListener;
import ru.whitetigersoft.elearningandroid.Model.App.App;
import ru.whitetigersoft.elearningandroid.Model.BaseFragment.BaseFragment;
import ru.whitetigersoft.elearningandroid.Model.CustomListeners.OnEnableRecycleViewListener;
import ru.whitetigersoft.elearningandroid.Model.DataProviders.BaseDataProvider;
import ru.whitetigersoft.elearningandroid.Model.DataProviders.DataProviderListener.BaseDataProviderListener;
import ru.whitetigersoft.elearningandroid.Model.DataProviders.LessonDataProvider;
import ru.whitetigersoft.elearningandroid.Model.Helpers.DateHelper;
import ru.whitetigersoft.elearningandroid.Model.Helpers.ItemClickSupport;
import ru.whitetigersoft.elearningandroid.Model.Helpers.ListHelper;
import ru.whitetigersoft.elearningandroid.Model.Helpers.StringHelper;
import ru.whitetigersoft.elearningandroid.Model.Models.Course;
import ru.whitetigersoft.elearningandroid.Model.Models.Lesson;
import ru.whitetigersoft.elearningandroid.R;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Александр on 20.07.2017.
 */

public class CoursesDetailFragment extends BaseFragment {

    static final private int LESSON_ACTIVITY_RESULT = 0;
    private static final String IS_REFRESH_LESSON = "isRefreshLesson";
    private static final String BUNDLE_COURSE_ID_KEY = "bundleCourseIdKey";
    private static final String BUNDLE_LESSON = "bundleLesson";
    private View rootView;
    private Context context;
    private RecycleCourseDetailListAdapter recycleCourseDetailListAdapter;
    private RelativeLayout relativeLayoutMain;
    private LessonDataProvider lessonDataProvider;
    private int courseId;
    private boolean isEnableLessons;
    private Lesson currentLesson;
    private ArrayList<Integer> disablePositions;
    private LinearLayout loadingPanel;

    @Override
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        this.context = getActivity();
        Bundle bundle = getArguments();
        if (bundle != null) {
            courseId = bundle.getInt(BUNDLE_COURSE_ID_KEY);
        }
        disablePositions = new ArrayList<>();
        lessonDataProvider = new LessonDataProvider();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setTitle(getString(R.string.title_courses_detail));
        if (!ListHelper.isEmpty(App.getInstance().getCourseManager().getDisablePositions())) {
            disablePositions = App.getInstance().getCourseManager().getDisablePositions();
        }
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_detail_courses, container, false);
            setupRecyclerView();
            loadDataCourseDetail();
        }
        return rootView;
    }

    private void fillFieldOfData(Course courseDetail) {
        List<Object> objectList = new ArrayList<>();
        objectList.add(courseDetail);
        if (courseDetail.isStarted()) {
            isEnableLessons = true;
        }
        recycleCourseDetailListAdapter.setObjectList(objectList);
    }

    private void loadDataCourseDetail() {
        showProgressBarWithLongerDuration(getString(R.string.progress_bar_text_course_detail));
        App.getInstance().getApi().getCoursesApi().getCourseDetail(courseId, new CoursesApiListener() {
            @Override
            public void onCourseDetailLoaded(Course courseDetail) {
                if (!StringHelper.isEmpty(courseDetail.getTitle())) {
                    relativeLayoutMain.setVisibility(View.VISIBLE);
                    App.getInstance().getCourseManager().setCourse(courseDetail);
                    fillFieldOfData(courseDetail);
                    reloadDataLesson();
                }
            }

            @Override
            public void onFailure() {
                onFailure(context.getString(R.string.snack_bar_message_error_course_detail));
            }

            @Override
            public void onFailure(String errorString) {
                hideProgressBar();
                showSnackBarError(rootView, errorString, null);
            }
        });
    }

    private void reloadDataLesson() {
        showProgressBarWithLongerDuration(getString(R.string.progress_bar_text_list_lesson));
        lessonDataProvider.reloadData();
    }

    private BaseDataProviderListener getLessonDataProvider() {
        return new BaseDataProviderListener() {
            @Override
            public void onItemsLoaded(List<?> items) {
                hideProgressBar();
                App.getInstance().getCourseManager().setLessonList((List<Lesson>) items);
                recycleCourseDetailListAdapter.addToObjectList((List<Object>) items);
            }

            @Override
            public void onEmpty() {
                hideProgressBar();
                relativeLayoutMain.setVisibility(View.VISIBLE);
                recycleCourseDetailListAdapter.addEmptyValue(getString(R.string.text_view_lessons_empty));
            }

            @Override
            public void onError() {
                onError(context.getString(R.string.snack_bar_message_error_lesson));
            }

            @Override
            public void onError(String errorString) {
                hideProgressBar();
                showSnackBarError(rootView, errorString, lessonDataProvider);
            }
        };
    }

    @Override
    public void showSnackBarError(View view, String text, final BaseDataProvider baseDataProvider) {
        if (getContext() != null) {
            Snackbar snackbar = getSnackbar(view, text, SNACK_BAR_ERROR_DURATION)
                    .setAction(getString(R.string.snackbar_button_reload), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (getContext() != null) {
                                relativeLayoutMain.setVisibility(View.GONE);
                                loadDataCourseDetail();
                                lessonDataProvider.reloadData();
                            }
                        }
                    });
            snackbar.show();
        }
    }

    private boolean isDisablePosition(int position) {
        if (disablePositions.size() == 0) {
            return false;
        }
        for (Integer mPosition : disablePositions) {
            if (mPosition.equals(position)) {
                return true;
            }
        }
        return false;
    }

    private void startLesson(Lesson lessonApp) {
        currentLesson = lessonApp;
        Intent intent = new Intent(context, LessonActivity.class);
        intent.putExtra(BUNDLE_LESSON, lessonApp);
        intent.putExtra(BUNDLE_COURSE_ID_KEY, courseId);
        startActivityForResult(intent, LESSON_ACTIVITY_RESULT);
    }

    private ItemClickSupport.OnItemClickListener createItemClickSupport() {
        return new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View view) {
                Object object = recycleCourseDetailListAdapter.getItem(position);
                if (object.getClass() == Lesson.class && isEnableLessons) {
                    if (!isDisablePosition(position)) {
                        final Lesson lessonApp = (Lesson) recycleCourseDetailListAdapter.getObjectList().get(position);
                        if (lessonApp.getType() == Lesson.TYPE_TEST) {
                            App.getInstance().getApi().getTestApi().checkPassingTest(lessonApp.getTestLessonId(), new TestApiListener() {
                                @Override
                                public void onCheckTestPassing(Integer remainingTime) {
                                    if (remainingTime > 0) {
                                        Date date = new Date();
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.setTimeInMillis(date.getTime() + (remainingTime * 1000));
                                        String stringDate = DateHelper.getTimeStringFromCalendar(calendar);
                                        String text = context.getString(R.string.constant_remaining_time_message) + " " + stringDate;
                                        Snackbar.make(rootView, text, Snackbar.LENGTH_SHORT).show();
                                    } else {
                                        startLesson(lessonApp);
                                    }
                                }

                                @Override
                                public void onFailure() {
                                    onFailure(context.getString(R.string.snack_bar_message_error_test));
                                }

                                @Override
                                public void onFailure(String errorString) {
                                    showSnackBarError(rootView, errorString, null);
                                }
                            });
                        } else {
                            startLesson(lessonApp);
                        }
                    }
                }
            }
        };
    }

    private boolean allLessonsIsCompleted(List<Lesson> lessonList) {
        int count = 0;
        for (Lesson lesson : lessonList) {
            if (lesson.isCompleted() != null && lesson.isCompleted()) {
                count++;
            }
        }
        return count == lessonList.size();
    }

    private void refreshDetailCourse() {
        // сохраняем текущую позицию урока
        final int index = App.getInstance().getCourseManager().findIndexByLesson(currentLesson);
        relativeLayoutMain.setVisibility(View.GONE);
        // обновляем данные
        loadDataCourseDetail();
        App.getInstance().getApi().getLessonApi().getLessons(courseId, new LessonApiListener() {
            @Override
            public void onListLessonLoaded(List<Lesson> lessonList) {
                App.getInstance().getCourseManager().setLessonList(lessonList);
                recycleCourseDetailListAdapter.addToObjectListLessons(lessonList);
                // возращаем новые данные по сохраненному индексу
                Lesson previousLesson;
                if (index > 0) {
                    previousLesson = lessonList.get(index - 1);
                } else {
                    previousLesson = lessonList.get(0);
                }
                // если заблокированная позиция содержит следущую позицию от текущей и текущий урок выполнен, то удаляем заблокированную позицию
                if (previousLesson != null && previousLesson.isCompleted() != null && previousLesson.isCompleted() && !ListHelper.isEmpty(disablePositions)) {
                    disablePositions.remove(0);
                }

                if (allLessonsIsCompleted(lessonList) && disablePositions.size() > 0) {
                    disablePositions.clear();
                }
                App.getInstance().getCourseManager().setDisablePositions(disablePositions);
                relativeLayoutMain.setVisibility(View.VISIBLE);
                relativeLayoutMain.bringToFront();
            }

            @Override
            public void onFailure() {
                super.onFailure(context.getString(R.string.snack_bar_message_error_lesson));
            }

            @Override
            public void onFailure(String errorString) {
                hideProgressBar();
                showSnackBarError(rootView, errorString, lessonDataProvider);
            }
        });
    }

    @Override
    public void showProgressBarWithLongerDuration(String text) {
        loadingPanel.setVisibility(View.VISIBLE);
        loadingPanel.bringToFront();
    }

    @Override
    public void hideProgressBar() {
        loadingPanel.setVisibility(View.GONE);
    }

    private void setupRecyclerView() {
        RecyclerView recycleViewStatisticCourseDetail = (RecyclerView) rootView.findViewById(R.id.recycle_view_course_detail);
        this.relativeLayoutMain = (RelativeLayout) rootView.findViewById(R.id.relative_layout_main_course_detail);
        this.loadingPanel = (LinearLayout) rootView.findViewById(R.id.loading_panel);
        this.recycleCourseDetailListAdapter = new RecycleCourseDetailListAdapter(loadingPanel, context, new OnEnableRecycleViewListener() {
            @Override
            public void onEnable() {
                isEnableLessons = true;
                relativeLayoutMain.setVisibility(View.GONE);
                loadDataCourseDetail();
                reloadDataLesson();
            }

            @Override
            public void onDisable(int position) {
                if (position != 0 && !disablePositions.contains(position)) {
                    disablePositions.add(position);
                    App.getInstance().getCourseManager().setDisablePositions(disablePositions);
                }
            }

            @Override
            public void onFailure() {
                onFailure(getString(R.string.snack_bar_message_error_start_course));
            }

            @Override
            public void onFailure(String errorString) {
                relativeLayoutMain.setVisibility(View.GONE);
                showSnackBarError(rootView, errorString, lessonDataProvider);
            }
        });
        ItemClickSupport.addTo(recycleViewStatisticCourseDetail).setOnItemClickListener(createItemClickSupport());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        lessonDataProvider.setId(courseId);
        lessonDataProvider.setBaseDataProvider(getLessonDataProvider());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycleViewStatisticCourseDetail.setLayoutManager(layoutManager);
        recycleViewStatisticCourseDetail.setAdapter(recycleCourseDetailListAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!ListHelper.isEmpty(App.getInstance().getCourseManager().getDisablePositions())) {
            App.getInstance().getCourseManager().clearDisablePositions();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LESSON_ACTIVITY_RESULT) {
            if (resultCode == RESULT_OK) {
                if (data.hasExtra(IS_REFRESH_LESSON) && data.getBooleanExtra(IS_REFRESH_LESSON, false)) {
                    refreshDetailCourse();
                }
            }
        }
    }
}
