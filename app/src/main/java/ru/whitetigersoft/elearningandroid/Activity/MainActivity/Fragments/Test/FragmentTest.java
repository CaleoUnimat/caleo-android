package ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.Test;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.Result.ResultFragment;
import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.TestApiListener;
import ru.whitetigersoft.elearningandroid.Model.App.App;
import ru.whitetigersoft.elearningandroid.Model.BaseFragment.BaseFragment;
import ru.whitetigersoft.elearningandroid.Model.Helpers.ListHelper;
import ru.whitetigersoft.elearningandroid.Model.Helpers.StringHelper;
import ru.whitetigersoft.elearningandroid.Model.Models.Answers;
import ru.whitetigersoft.elearningandroid.Model.Models.Lesson;
import ru.whitetigersoft.elearningandroid.Model.Models.Question;
import ru.whitetigersoft.elearningandroid.Model.Models.Test;
import ru.whitetigersoft.elearningandroid.R;
import ru.whitetigersoft.elearningandroid.View.CustomRadioButton;

/**
 * Created by Александр on 21.07.2017.
 */

public class FragmentTest extends BaseFragment {

    private static final int TIMER_STEP = 1000;
    private static final int PRELOADER_SHOW_TIME = 1000;
    private static final String BUNDLE_COURSE_TEST_KEY = "bundleLessonTestKey";
    private static final String BUNDLE_COUNT_QUESTION_KEY = "bundleLessonCountQuestionKey";
    private static final String BUNDLE_ANSWERS_PASS_KEY = "bundleAnswersPassKey";
    private static final String BUNDLE_RESULT_LESSON = "bundleResultLessonKey";
    private Context context;
    private TextView textViewTextQuestion;
    private TextView textViewProgressTest;
    private ScrollView layoutMainTest;
    private TextView textViewTextChoose;
    private EditText editTextAnswer;
    private Test mTest;
    private String answersForPassTest;
    private Lesson lesson;
    private int countOfQuestion;
    private Button buttonAnswer;
    private View rootView;

    @Override
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        this.context = getActivity();
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(BUNDLE_COURSE_TEST_KEY)) {
                this.mTest = bundle.getParcelable(BUNDLE_COURSE_TEST_KEY);
            }
            if (bundle.containsKey(BUNDLE_ANSWERS_PASS_KEY)) {
                this.answersForPassTest = bundle.getString(BUNDLE_ANSWERS_PASS_KEY);
            }
            if (bundle.containsKey(BUNDLE_COUNT_QUESTION_KEY)) {
                this.countOfQuestion = bundle.getInt(BUNDLE_COUNT_QUESTION_KEY);
            }
            this.lesson = bundle.getParcelable(BUNDLE_RESULT_LESSON);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        rootView = inflater.inflate(R.layout.fragment_test, container, false);
        RadioGroup radioGroupTest = (RadioGroup) rootView.findViewById(R.id.radio_group_test);
        String title = getString(R.string.title_test) + ": " + this.lesson.getTitle();
        setTitle(title);
        prepareViews(rootView, radioGroupTest);
        loadData(radioGroupTest);
        return rootView;
    }

    private void fillRadioGroupQuestions(final RadioGroup radioGroupTest, List<Answers> answerses) {
        radioGroupTest.clearCheck();
        int marginBottom = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 23, context.getResources().getDisplayMetrics());
        int marginLeft = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 11, context.getResources().getDisplayMetrics());
        int marginRight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 11, context.getResources().getDisplayMetrics());
        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 44, context.getResources().getDisplayMetrics());
        for (Answers answer : answerses) {
            CustomRadioButton newRadioButton = new CustomRadioButton(context);
            newRadioButton.setText(answer.getAnswer());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height);
            layoutParams.setMargins(marginLeft, 0, marginRight, marginBottom);
            radioGroupTest.addView(newRadioButton);
            newRadioButton.setLayoutParams(layoutParams);
            newRadioButton.setOnClickListener(onRadioButtonClickListener());
        }
    }

    private void fillTestData(final RadioGroup radioGroupTest, List<Question> questions, int countOfQuestion) {
        buttonAnswer.setEnabled(true);
        textViewProgressTest.setText(getString(R.string.constant_question) + " " + (countOfQuestion + 1) + " " + context.getString(R.string.constant_from) + " " + questions.size());
        try {
            if (!ListHelper.isEmpty(questions) && !StringHelper.isEmpty(questions.get(countOfQuestion).getQuestion())) {
                textViewTextQuestion.setText(questions.get(countOfQuestion).getQuestion());
                if (!ListHelper.isEmpty(questions.get(countOfQuestion).getAnswers())) {
                    editTextAnswer.setVisibility(View.GONE);
                    radioGroupTest.setVisibility(View.VISIBLE);
                    textViewTextChoose.setText(getString(R.string.text_view_test_multiple_choose));
                    fillRadioGroupQuestions(radioGroupTest, questions.get(countOfQuestion).getAnswers());
                } else if (!StringHelper.isEmpty(questions.get(countOfQuestion).getAnswerString())) {
                    editTextAnswer.setVisibility(View.VISIBLE);
                    radioGroupTest.setVisibility(View.GONE);
                    textViewTextChoose.setText(getString(R.string.text_view_test_one_choose));
                }
            }
        } catch (Exception e) {
            Toast.makeText(context, context.getString(R.string.toast_message_error_on_load), Toast.LENGTH_SHORT).show();
        }
    }

    private void loadData(final RadioGroup radioGroupTest) {
        if (mTest == null) {
            showProgressBarWithLongerDuration(getString(R.string.progress_bar_text_test));
            layoutMainTest.setVisibility(View.GONE);
            App.getInstance().getApi().getTestApi().getTests(lesson.getLessonId(), new TestApiListener() {
                @Override
                public void onTestLoaded(Test test) {
                    hideProgressBar();
                    layoutMainTest.setVisibility(View.VISIBLE);
                    mTest = test;
                    List<Question> questions = test.getQuestions();
                    fillTestData(radioGroupTest, questions, 0);
                }

                @Override
                public void onFailure() {
                    onFailure(context.getString(R.string.snack_bar_message_error_test));
                }

                @Override
                public void onFailure(String errorString) {
                    showSnackBarError(rootView, errorString, radioGroupTest);
                }
            });
        } else {
            countOfQuestion++;
            List<Question> questions = mTest.getQuestions();
            fillTestData(radioGroupTest, questions, countOfQuestion);
        }
    }

    private void startTestFragment(Bundle bundle) {
        FragmentTest fragmentTest = new FragmentTest();
        fragmentTest.setArguments(bundle);
        replaceLessonFragment(fragmentTest);
    }

    private void startResultFragment() {
        ResultFragment resultFragment = new ResultFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_RESULT_LESSON, lesson);
        bundle.putString(BUNDLE_ANSWERS_PASS_KEY, answersForPassTest);
        resultFragment.setArguments(bundle);
        replaceLessonFragment(resultFragment);
    }

    private void startTimer(final int answerId, final int questionId, final String answer, final int type) {
        new CountDownTimer(PRELOADER_SHOW_TIME, TIMER_STEP) {
            @Override
            public void onTick(long timer) {
                if (getView() == null) {
                    cancel();
                }
            }

            @Override
            public void onFinish() {
                if (getView() != null) {
                    passTest(answerId, questionId, answer, type);
                    if (countOfQuestion == mTest.getQuestions().size() - 1) {
                        startResultFragment();
                    } else {
                        startNextTest();
                    }
                }
            }
        }.start();
    }

    private void passTest(int answerId, int questionId, String answer, int type) {
        if (StringHelper.isEmpty(answersForPassTest)) {
            answersForPassTest = "[";
        }
        if (type == Test.TYPE_MULTIPLE && answerId != 0 && questionId != 0) {
            answersForPassTest += "{\"" + questionId + "\":" + answerId + "}";
        } else if (type == Test.TYPE_SINGLE && questionId != 0 && !StringHelper.isEmpty(answer)) {
            answersForPassTest += "{\"" + questionId + "\":\"" + answer + "\"}";
        }
        if (mTest != null) {
            if (countOfQuestion + 1 < mTest.getQuestions().size()) {
                answersForPassTest += ',';
            } else {
                answersForPassTest += ']';
            }
        }
    }

    private void startNextTest() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_COURSE_TEST_KEY, mTest);
        bundle.putInt(BUNDLE_COUNT_QUESTION_KEY, countOfQuestion);
        bundle.putParcelable(BUNDLE_RESULT_LESSON, lesson);
        bundle.putString(BUNDLE_ANSWERS_PASS_KEY, answersForPassTest);
        startTestFragment(bundle);
    }

    private void setRadioButtonCustomColor(View buttonView, RadioGroup radioGroupTest) {
        int idRadioButton = radioGroupTest.getCheckedRadioButtonId();
        if (idRadioButton != -1) {
            buttonView.setEnabled(false);
            CustomRadioButton radioButton = (CustomRadioButton) rootView.findViewById(idRadioButton);
            Question question = mTest.getQuestions().get(countOfQuestion);
            Answers answers = question.getAnswers().get(radioGroupTest.indexOfChild(radioButton));
            radioButton.setTextColor(ContextCompat.getColor(context, android.R.color.white));
            if (answers.isRightAnswer()) {
                radioButton.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_radio_button_with_green_fill));
                startTimer(answers.getTestQuestionAnswerId(), question.getTestQuestionId(), null, Test.TYPE_MULTIPLE);
            } else {
                radioButton.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_radio_button_with_red_fill));
                startTimer(answers.getTestQuestionAnswerId(), question.getTestQuestionId(), null, Test.TYPE_MULTIPLE);
            }
        } else {
            Snackbar.make(rootView, getString(R.string.snack_bar_message_error_test_answer), Snackbar.LENGTH_LONG).show();
        }
    }

    private void setEditTextCustomColor(View buttonView) {
        String answer = editTextAnswer.getText().toString().trim();
        if (StringHelper.isEmpty(answer)) {
            Snackbar.make(rootView, getString(R.string.snack_bar_message_error_test_answer_edit_text), Snackbar.LENGTH_LONG).show();
        } else {
            buttonView.setEnabled(false);
            Question question = mTest.getQuestions().get(countOfQuestion)   ;
            int editTextPadding = editTextAnswer.getPaddingLeft();
            buttonAnswer.setTextColor(ContextCompat.getColor(context, android.R.color.white));
            if (editTextAnswer.getText().toString().equalsIgnoreCase(mTest.getQuestions().get(countOfQuestion).getAnswerString())) {
                editTextAnswer.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_edit_text_with_green_round));
                editTextAnswer.setPadding(editTextPadding, 0, editTextPadding, 0);
                editTextAnswer.setTextColor(ContextCompat.getColor(context, R.color.color_apple_green));
                buttonAnswer.setBackground(ContextCompat.getDrawable(context, R.drawable.button_background_circle_fill_green));
                startTimer(0, question.getTestQuestionId(), editTextAnswer.getText().toString(), Test.TYPE_SINGLE);
            } else {
                editTextAnswer.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_edit_text_with_red_round));
                editTextAnswer.setPadding(editTextPadding, 0, editTextPadding, 0);
                editTextAnswer.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                buttonAnswer.setBackground(ContextCompat.getDrawable(context, R.drawable.button_background_circle_fill_red));
                startTimer(0, question.getTestQuestionId(), editTextAnswer.getText().toString(), Test.TYPE_SINGLE);
            }
        }
    }

    private View.OnClickListener onButtonAnswerClickListener(final RadioGroup radioGroupTest) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ListHelper.isEmpty(mTest.getQuestions().get(countOfQuestion).getAnswers())) {
                    setRadioButtonCustomColor(v, radioGroupTest);
                } else if (!StringHelper.isEmpty(mTest.getQuestions().get(countOfQuestion).getAnswerString())) {
                    setEditTextCustomColor(v);
                }
            }
        };
    }

    public void showSnackBarError(final View rootView, String text, final RadioGroup radioGroupTest) {
        if (getContext() != null) {
            Snackbar snackbar = getSnackbar(rootView, text, SNACK_BAR_ERROR_DURATION)
                    .setAction(getString(R.string.snackbar_button_reload), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (getContext() != null) {
                                showProgressBarWithLongerDuration(getString(R.string.progress_bar_text_test));
                                loadData(radioGroupTest);
                            }
                        }
                    });
            snackbar.show();
        }
    }

    private void prepareViews(View rootView, final RadioGroup radioGroupTest) {
        this.textViewProgressTest = (TextView) rootView.findViewById(R.id.text_view_progress_test);
        this.textViewTextQuestion = (TextView) rootView.findViewById(R.id.text_view_text_question);
        this.textViewTextChoose = (TextView) rootView.findViewById(R.id.text_view_text_choose);
        this.editTextAnswer = (EditText) rootView.findViewById(R.id.edit_text_answer);
        this.layoutMainTest = (ScrollView) rootView.findViewById(R.id.layout_main_test);
        this.buttonAnswer = (Button) rootView.findViewById(R.id.button_answer);
        this.editTextAnswer.setOnClickListener(onEditTextClickListener());
        this.editTextAnswer.setOnFocusChangeListener(onEditTextFocusListener());
        this.editTextAnswer.setOnEditorActionListener(onEditorActionListener());
        buttonAnswer.setOnClickListener(onButtonAnswerClickListener(radioGroupTest));
    }

    public EditText.OnEditorActionListener onEditorActionListener() {
        return new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideKeyboard();
                    return true;
                }
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        };
    }

    private View.OnFocusChangeListener onEditTextFocusListener() {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    buttonAnswer.setVisibility(View.VISIBLE);
                }
            }
        };
    }

    private View.OnClickListener onEditTextClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              buttonAnswer.setVisibility(View.VISIBLE);
            }
        };
    }


    private View.OnClickListener onRadioButtonClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonAnswer.setVisibility(View.VISIBLE);
            }
        };
    }
}
