package ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.PdfView;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnPageScrollListener;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import permission.auron.com.marshmallowpermissionhelper.PermissionResult;
import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.LessonApiListener;
import ru.whitetigersoft.elearningandroid.Model.App.App;
import ru.whitetigersoft.elearningandroid.Model.BaseFragment.BaseFragment;
import ru.whitetigersoft.elearningandroid.Model.DataProviders.BaseDataProvider;
import ru.whitetigersoft.elearningandroid.Model.Helpers.ListHelper;
import ru.whitetigersoft.elearningandroid.Model.Helpers.StringHelper;
import ru.whitetigersoft.elearningandroid.Model.Models.Lesson;
import ru.whitetigersoft.elearningandroid.Model.Utils.CustomUrlUtil;
import ru.whitetigersoft.elearningandroid.R;

/**
 * Created by Александр on 24.07.2017.
 */

public class PdfViewFragment extends BaseFragment {

    private static final String BUNDLE_PDF_KEY = "bundlePdfKey";
    private static final String BUNDLE_COURSE_ID_KEY = "bundleCourseIdKey";
    private Context context;
    private Lesson lesson;
    private int courseId;
    private ProgressDialog pDialog;
    private View rootView;
    private boolean isGoBack;
    private Button buttonNextLesson;
    private boolean isPassedPdfLesson;

    @Override
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        this.context = getActivity();
        Bundle bundle = getArguments();
        if (bundle != null) {
            lesson = bundle.getParcelable(BUNDLE_PDF_KEY);
            courseId = bundle.getInt(BUNDLE_COURSE_ID_KEY);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        rootView = inflater.inflate(R.layout.fragment_pdf, container, false);
        if (lesson.getTitle() != null) {
            setTitle(getString(R.string.constant_title_lesson) + ": " + lesson.getTitle());
        } else {
            setTitle(getString(R.string.title_pdf));
        }
        downloadPdfFile();
        return rootView;
    }

    private void downloadPdfFile() {
        askCompactPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, new PermissionResult() {
            @Override
            public void permissionGranted() {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                if (lesson.getPdfUrl() != null) {
                    new DownloadFileFromURL().execute(lesson.getPdfUrl());
                } else {
                    Toast.makeText(context, getString(R.string.snack_bar_message_error_pdf_file), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void permissionDenied() {
                isGoBack = true;
            }

            @Override
            public void permissionForeverDenied() {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isGoBack) {
            exitFromLesson();
        }
    }


    private View.OnClickListener onButtonNextClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNextLesson();
            }
        };
    }

    private void prepareViews(String file) {
        PDFView pdfView = (PDFView) rootView.findViewById(R.id.pdfView);
        buttonNextLesson = (Button) rootView.findViewById(R.id.button_next_lesson);
        buttonNextLesson.setOnClickListener(onButtonNextClickListener());
        if (!StringHelper.isEmpty(file)) {
            pdfView.fromFile(new File(file))
                    .enableSwipe(true)
                    .password(null)
                    .onPageScroll(onPageScrollListener())
                    .load();
        } else {
            Snackbar.make(rootView, getString(R.string.snack_bar_message_error_pdf_file_download), Snackbar.LENGTH_LONG).show();
        }
    }

    private void passTextLesson() {
        App.getInstance().getApi().getLessonApi().passTextLesson(lesson.getLessonId(), new LessonApiListener() {
            @Override
            public void onSuccess() {
                String title;
                List<Lesson> lessonList = App.getInstance().getCourseManager().getLessonList();
                int index = App.getInstance().getCourseManager().findIndexByLesson(lessonList, lesson);
                if (index + 1 < App.getInstance().getCourseManager().getLessonList().size()) {
                    title = getString(R.string.button_title_next_lesson);
                } else {
                    title = getString(R.string.button_title_finish_lesson);
                }
                buttonNextLesson.setText(title);
                buttonNextLesson.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure() {
                onFailure(context.getString(R.string.snack_bar_message_error_pass_test));
                isPassedPdfLesson = false;
            }

            @Override
            public void onFailure(String errorString) {
                showSnackBarError(rootView, errorString, null);
                isPassedPdfLesson = false;
            }
        });
    }

    private OnPageScrollListener onPageScrollListener() {
        return new OnPageScrollListener() {
            @Override
            public void onPageScrolled(int page, float positionOffset) {
                if (positionOffset == 1) {
                    if (!isPassedPdfLesson) {
                        isPassedPdfLesson = true;
                        passTextLesson();
                    }
                }
            }
        };
    }

    public void showSnackBarError(View view, String text, final BaseDataProvider baseDataProvider) {
        if (getContext() != null) {
            Snackbar snackbar = getSnackbar(view, text, SNACK_BAR_ERROR_DURATION)
                    .setAction(getString(R.string.snackbar_button_reload), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (getContext() != null) {
                                passTextLesson();
                            }
                        }
                    });
            snackbar.show();
        }
    }

    private void startNextLesson() {
        List<Lesson> lessonList = App.getInstance().getCourseManager().getLessonList();
        int index = App.getInstance().getCourseManager().findIndexByLesson(lessonList, lesson);
        if (index + 1 < lessonList.size()) {
            Lesson nextLesson = App.getInstance().getCourseManager().getLessonList().get(index + 1);
            if (!ListHelper.isEmpty(App.getInstance().getCourseManager().getDisablePositions())) {
                App.getInstance().getCourseManager().removePosition(0);
            }
            if (nextLesson.getType() == Lesson.TYPE_TEXT && lesson != null) {
                App.getInstance().getCourseManager().setCurrentLesson(lesson);
                startPdfView(nextLesson);
            } else if (nextLesson.getType() == Lesson.TYPE_TEST && lesson != null) {
                App.getInstance().getCourseManager().setCurrentLesson(lesson);
                startTestFragment(nextLesson, courseId);
            } else if (nextLesson.getType() == Lesson.TYPE_VIDEO && lesson != null) {
                App.getInstance().getCourseManager().setCurrentLesson(lesson);
                startYouTube(nextLesson, courseId);
            }
        } else {
            exitFromLesson();
        }
    }

    private void onCreateDialog() {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(getString(R.string.progress_dialog_text_pdf));
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(true);
        pDialog.show();
    }

    private class DownloadFileFromURL extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            onCreateDialog();
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            URL url;
            try {
                url = new URL(f_url[0]);
                String fileName = CustomUrlUtil.getLastNameOfUrl(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                int lengthOfFile = connection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream());
                String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + fileName;
                OutputStream output = new FileOutputStream(filePath);

                byte data[] = new byte[1024 * 1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();
                return filePath;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            prepareViews(file_url);
        }
    }
}
