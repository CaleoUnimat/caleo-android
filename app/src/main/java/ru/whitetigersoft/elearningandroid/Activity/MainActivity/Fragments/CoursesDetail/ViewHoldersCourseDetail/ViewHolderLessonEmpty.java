package ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.CoursesDetail.ViewHoldersCourseDetail;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ru.whitetigersoft.elearningandroid.R;

/**
 * Created by Александр on 19.07.2017.
 */

public class ViewHolderLessonEmpty extends RecyclerView.ViewHolder {

    private TextView textViewEmpty;

    public ViewHolderLessonEmpty(View itemView) {
        super(itemView);
        this.textViewEmpty = (TextView) itemView.findViewById(R.id.text_view_empty_lesson);
    }

    public TextView getTextViewEmpty() {
        return textViewEmpty;
    }
}
