package ru.whitetigersoft.elearningandroid.Activity.Login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import ru.whitetigersoft.elearningandroid.Activity.MainActivity.MainActivity;
import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.LoginApiListener;
import ru.whitetigersoft.elearningandroid.Model.App.App;
import ru.whitetigersoft.elearningandroid.Model.Helpers.StringHelper;
import ru.whitetigersoft.elearningandroid.R;

/**
 * Created by Александр on 19.07.2017.
 */

public class LoginActivity extends AppCompatActivity {

    private final static int SNACK_BAR_ERROR_DURATION = 20000;
    private EditText editTextPassword;
    private EditText editTextLogin;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.setContentView(R.layout.activity_authorization);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar_splash_activity);
        setTitle(getString(R.string.title_login));
        prepareView();
    }

    private void loadData() {
        showProgressBar();
        String login = editTextLogin.getText().toString();
        String password = editTextPassword.getText().toString();
        App.getInstance().getApi().getLoginApi().loginUser(login, password, new LoginApiListener() {
            @Override
            public void loginSuccess(String accessToken, boolean isAlreadyRegistered) {
                App.getInstance().getAppUser().setAccessToken(accessToken);
                startMainActivity();
            }

            @Override
            public void onSuccess() {
                startMainActivity();
            }

            @Override
            public void onFailure() {
                onFailure(getString(R.string.snack_bar_message_error_authorization));
            }

            @Override
            public void onFailure(String errorString) {
                hideProgressBar();
                showSnackBarError(findViewById(android.R.id.content), errorString);
            }
        });
    }

    private void prepareView() {
        this.editTextPassword = (EditText) findViewById(R.id.edit_text_password);
        this.editTextLogin = (EditText) findViewById(R.id.edit_text_login);
        Button buttonLogin = (Button) findViewById(R.id.button_login);
        buttonLogin.setOnClickListener(onButtonLoginClickListener());
    }


    private void startMainActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private View.OnClickListener onButtonLoginClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInputOnCorrect()) {
                    v.setEnabled(false);
                    loadData();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_message_error_input_authorization), Toast.LENGTH_LONG).show();
                }
            }
        };
    }

    private boolean checkInputOnCorrect() {
        return !StringHelper.isEmpty(editTextLogin.getText().toString()) && !StringHelper.isEmpty(editTextPassword.getText().toString());
    }

    private void showProgressBar() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        progressBar.bringToFront();
        progressBar.startAnimation(animation);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        progressBar.startAnimation(animation);
        progressBar.setVisibility(View.GONE);
    }

    public Snackbar getSnackbar(View rootView, String message, int duration) {
        Snackbar snackbar = Snackbar.make(rootView, message, duration);
        TextView textView = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setSingleLine(false);
        return snackbar;
    }

    public void showSnackBarError(View rootView, String text) {
        if (rootView != null) {
            Snackbar snackbar = getSnackbar(rootView, text, SNACK_BAR_ERROR_DURATION)
                    .setAction(getString(R.string.snackbar_button_reload), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (getApplicationContext() != null) {
                                showProgressBar();
                                loadData();
                            }
                        }
                    });
            snackbar.show();
        } else {
            Toast.makeText(this, text, Toast.LENGTH_LONG).show();
        }
    }
}
