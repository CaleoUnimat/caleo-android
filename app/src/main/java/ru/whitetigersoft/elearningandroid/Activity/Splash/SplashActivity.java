package ru.whitetigersoft.elearningandroid.Activity.Splash;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import ru.whitetigersoft.elearningandroid.Activity.Login.LoginActivity;
import ru.whitetigersoft.elearningandroid.Activity.MainActivity.MainActivity;
import ru.whitetigersoft.elearningandroid.Model.App.App;
import ru.whitetigersoft.elearningandroid.R;

/**
 * Created by Александр on 19.07.2017.
 */

public class SplashActivity extends AppCompatActivity {

    private static final int TIMER_STEP = 1000;
    private static final int PRELOADER_SHOW_TIME = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        Fabric.with(this, new Crashlytics());
        showProgressBar();
        App.createInstance(this);
        startTimer();
    }

    private void showProgressBar() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar_splash_activity);
        progressBar.bringToFront();
        progressBar.startAnimation(animation);
        progressBar.setVisibility(View.VISIBLE);
    }


    private void startTimer() {
        new CountDownTimer(PRELOADER_SHOW_TIME, TIMER_STEP) {
            @Override
            public void onTick(long timer) {

            }

            @Override
            public void onFinish() {
                if (!App.getInstance().getAppUser().isAuthenticated()) {
                    startActivity(LoginActivity.class);
                } else {
                    startActivity(MainActivity.class);
                }
            }
        }.start();
    }

    private void startActivity(Class cls) {
        Intent intent = new Intent(getApplicationContext(), cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
