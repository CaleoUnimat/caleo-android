package ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.Courses;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import ru.whitetigersoft.elearningandroid.R;
import ru.whitetigersoft.elearningandroid.View.CustomTextView;

/**
 * Created by Александр on 19.07.2017.
 */

public class ViewHolderCourse extends RecyclerView.ViewHolder {

    private final TextView textViewProgressCourse;
    private final CustomTextView textViewNameCourse;
    private final ProgressBar progressBarCourse;
    private final ImageView imageViewCourses;

    public ViewHolderCourse(View itemView) {
        super(itemView);
        textViewProgressCourse = (TextView) itemView.findViewById(R.id.text_view_progress_course);
        textViewNameCourse = (CustomTextView) itemView.findViewById(R.id.text_view_name_course);
        progressBarCourse = (ProgressBar) itemView.findViewById(R.id.progress_bar_course);
        imageViewCourses = (ImageView) itemView.findViewById(R.id.image_view_courses);
    }

    public TextView getTextViewProgressCourse() {
        return textViewProgressCourse;
    }

    public CustomTextView getTextViewNameCourse() {
        return textViewNameCourse;
    }

    public ProgressBar getProgressBarCourse() {
        return progressBarCourse;
    }

    public ImageView getImageViewCourses() {
        return imageViewCourses;
    }
}
