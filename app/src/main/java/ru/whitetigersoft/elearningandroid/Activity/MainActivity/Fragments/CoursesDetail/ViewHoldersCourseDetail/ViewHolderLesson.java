package ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.CoursesDetail.ViewHoldersCourseDetail;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.whitetigersoft.elearningandroid.R;

/**
 * Created by Александр on 19.07.2017.
 */

public class ViewHolderLesson extends RecyclerView.ViewHolder {

    private ImageView imageViewTypeMaterial;
    private TextView textViewMaterialId;
    private TextView textViewNameMaterial;
    private TextView textViewStatusPassedMaterial;
    private ImageView imageViewIsCompleted;
    private LinearLayout linearLayoutLessonMain;

    public ViewHolderLesson(View itemView) {
        super(itemView);
        this.textViewStatusPassedMaterial = (TextView) itemView.findViewById(R.id.text_view_status_material);
        this.textViewNameMaterial = (TextView) itemView.findViewById(R.id.text_view_name_material);
        this.textViewMaterialId = (TextView) itemView.findViewById(R.id.text_view_material_id);
        this.imageViewTypeMaterial = (ImageView) itemView.findViewById(R.id.image_view_type_material);
        this.imageViewIsCompleted=  (ImageView) itemView.findViewById(R.id.image_view_is_completed);
        this.linearLayoutLessonMain = (LinearLayout) itemView.findViewById(R.id.linear_layout_lesson_main);
    }

    public LinearLayout getLinearLayoutLessonMain() {
        return linearLayoutLessonMain;
    }

    public ImageView getImageViewIsCompleted() {
        return imageViewIsCompleted;
    }

    public ImageView getImageViewTypeMaterial() {
        return imageViewTypeMaterial;
    }

    public TextView getTextViewMaterialId() {
        return textViewMaterialId;
    }

    public TextView getTextViewNameMaterial() {
        return textViewNameMaterial;
    }

    public TextView getTextViewStatusPassedMaterial() {
        return textViewStatusPassedMaterial;
    }
}
