package ru.whitetigersoft.elearningandroid.Activity.MainActivity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.Courses.CoursesListFragment;
import ru.whitetigersoft.elearningandroid.Model.BaseActivity.BaseActivity;
import ru.whitetigersoft.elearningandroid.R;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        startCourseFragment();
    }

    private void startCourseFragment() {
        CoursesListFragment coursesListFragment = new CoursesListFragment();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            coursesListFragment.setArguments(bundle);
        }
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, coursesListFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }
}
