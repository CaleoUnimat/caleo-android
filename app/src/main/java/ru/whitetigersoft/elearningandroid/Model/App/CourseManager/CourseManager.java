package ru.whitetigersoft.elearningandroid.Model.App.CourseManager;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ru.whitetigersoft.elearningandroid.Model.Models.Course;
import ru.whitetigersoft.elearningandroid.Model.Models.Lesson;

/**
 * Created by Александр on 08.08.2017.
 */
public class CourseManager {
    private static final String SAVED_COURSE = "courseSharedPreference";
    private static final String STORING_KEY_LIST = "storingKeyList";
    private static final String STORING_KEY_OBJECT = "storingKeyObj";
    private Context context;
    private SharedPreferences sharedPreferences;
    private ArrayList<Integer> disablePositions;
    private Lesson currentLesson;

    public CourseManager(Context context) {
        sharedPreferences = context.getSharedPreferences(SAVED_COURSE, Context.MODE_PRIVATE);
        this.context = context;
    }

    public void setLessonList(List<Lesson> lessonList) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String value = gson.toJson(lessonList);
        SharedPreferences prefs = context.getSharedPreferences(SAVED_COURSE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(STORING_KEY_LIST, value);
        editor.apply();
    }

    public void setCourse(Course course) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String value = gson.toJson(course);
        SharedPreferences prefs = context.getSharedPreferences(SAVED_COURSE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(STORING_KEY_OBJECT, value);
        editor.apply();
    }

    public List<Lesson> getLessonList() {
        String value = sharedPreferences.getString(STORING_KEY_LIST, null);
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        Type listType = new TypeToken<List<Lesson>>() {}.getType();
        List<Lesson> lessonList = gson.fromJson(value, listType);
        return lessonList;
    }

    public ArrayList<Integer> getDisablePositions() {
        return disablePositions;
    }

    public void removePosition(int position) {
        this.disablePositions.remove(position);
    }

    public void clearDisablePositions() {
        this.disablePositions.clear();
    }

    public void setDisablePositions(ArrayList<Integer> disablePositions) {
        this.disablePositions = disablePositions;
    }

    private int getIndex(List<Lesson> lessonList, Lesson lesson, boolean hasId, int id) {
        for(int index = 0; index < lessonList.size(); index++) {
            if (!hasId) {
                if (Objects.equals(lessonList.get(index).getTitle(), lesson.getTitle()) && lessonList.get(index).getLessonId() == lesson.getLessonId()) {
                    return index;
                }
            } else {
                if (lessonList.get(index).getLessonId() == id) {
                    return index;
                }
            }
        }
        return -1;
    }

    public int findIndexByLesson(List<Lesson> lessonList, Lesson lesson) {
        return getIndex(lessonList, lesson, false, 0);
    }

    public int findIndexByLesson(Lesson lesson) {
        List<Lesson> lessonList = getLessonList();
        return getIndex(lessonList, lesson, false, 0);
    }

    public Lesson getCurrentLesson() {
        return currentLesson;
    }

    public void setCurrentLesson(Lesson currentLesson) {
        this.currentLesson = currentLesson;
    }
}
