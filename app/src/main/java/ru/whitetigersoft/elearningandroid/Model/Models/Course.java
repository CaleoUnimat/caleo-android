package ru.whitetigersoft.elearningandroid.Model.Models;

/**
 * Created by Александр on 19.07.2017.
 */

public class Course extends BaseModel {
    private int courseId;
    private String title;
    private String description;
    private float progress;
    private int passedLessonCount;
    private Integer totalLessonCount;
    private String imageUrl;
    private boolean isStarted;

    public String getDescription() {
        return description;
    }

    public int getCourseId() {
        return courseId;
    }

    public String getTitle() {
        return title;
    }

    public float getProgress() {
        return progress;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public boolean isStarted() {
        return isStarted;
    }

    public int getPassedLessonCount() {
        return passedLessonCount;
    }

    public Integer getLessonCount() {
        return totalLessonCount;
    }
}
