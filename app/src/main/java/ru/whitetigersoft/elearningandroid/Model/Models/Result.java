package ru.whitetigersoft.elearningandroid.Model.Models;

/**
 * Created by Александр on 09.08.2017.
 */

public class Result extends BaseModel {
    private String title;
    private Boolean isCompleted;
    private int questionCount;
    private int correctAnswers;
    private float correctAnswersProgress;
    private int wrongAnswers;
    private float wrongAnswersProgress;

    public String getTitle() {
        return title;
    }

    public Boolean isCompleted() {
        return isCompleted;
    }

    public int getQuestionCount() {
        return questionCount;
    }

    public int getCorrectAnswers() {
        return correctAnswers;
    }

    public float getCorrectAnswersProgress() {
        return correctAnswersProgress;
    }

    public int getWrongAnswers() {
        return wrongAnswers;
    }

    public float getWrongAnswersProgress() {
        return wrongAnswersProgress;
    }
}
