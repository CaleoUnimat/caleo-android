package ru.whitetigersoft.elearningandroid.Model.App;

import android.content.Context;

import java.util.List;

import ru.whitetigersoft.elearningandroid.Model.Api.Api;
import ru.whitetigersoft.elearningandroid.Model.App.AppUser.AppUser;
import ru.whitetigersoft.elearningandroid.Model.App.CourseManager.CourseManager;
import ru.whitetigersoft.elearningandroid.Model.Models.Lesson;

/**
 * Created by Александр on 19.07.2017.
 */

public class App {

    private static App instance;
    private AppUser appUser;
    private Api api;
    private CourseManager courseManager;

    public App(Context context) {
        this.api = new Api();
        this.courseManager = new CourseManager(context);
        this.appUser = new AppUser(context);
    }

    public static App getInstance() {
        synchronized (App.class) {
            return instance;
        }
    }

    public static void createInstance(Context context) {
        synchronized (App.class) {
            instance = new App(context);
        }
    }

    public Api getApi() {
        return api;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public CourseManager getCourseManager() {
        return courseManager;
    }
}
