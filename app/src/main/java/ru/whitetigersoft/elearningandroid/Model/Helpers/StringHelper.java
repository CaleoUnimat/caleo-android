package ru.whitetigersoft.elearningandroid.Model.Helpers;

/**
 * Created by valerijsamsonov on 29.05.17.
 */

public class StringHelper {
    public static boolean isEmpty(String string) {
        if (string == null || string.length() == 0) {
            return true;
        }
        return false;
    }

    public static String normalizePhoneNumber(String phoneNumber) {
        return phoneNumber.replaceAll("[^0-9]", "");
    }
}
