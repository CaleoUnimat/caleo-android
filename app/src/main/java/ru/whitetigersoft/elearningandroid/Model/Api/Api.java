package ru.whitetigersoft.elearningandroid.Model.Api;

import android.content.Context;

/**
 * Created by Александр on 19.07.2017.
 */

public class Api {

    private LoginApi loginApi;
    private CoursesApi coursesApi;
    private TestApi testApi;
    private LessonApi lessonApi;
    private ResultApi resultApi;

    public Api() {
        loginApi = new LoginApi();
        coursesApi = new CoursesApi();
        testApi = new TestApi();
        lessonApi = new LessonApi();
        resultApi = new ResultApi();
    }

    public LoginApi getLoginApi() {
        return loginApi;
    }

    public CoursesApi getCoursesApi() {
        return coursesApi;
    }

    public TestApi getTestApi() {
        return testApi;
    }

    public LessonApi getLessonApi() {
        return lessonApi;
    }

    public ResultApi getResultApi() {
        return resultApi;
    }
}
