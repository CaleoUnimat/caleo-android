package ru.whitetigersoft.elearningandroid.Model.Api;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.ApiListener;
import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.LoginApiListener;

/**
 * Created by Александр on 19.07.2017.
 */

public class LoginApi extends BaseApi {

    public static final String API_ACTION_LOGIN = "login-by-login-password";

    public void loginUser(String login, String password, final LoginApiListener loginApiListener) {
        RequestParams params = new RequestParams();
        params.put("login", login);
        params.put("password", password);
        String actionApi = API_CONTROLLER_COMMON + API_CONTROLLER_AUTH + API_ACTION_LOGIN;
        sendRequest(REQUEST_TYPE_POST, actionApi, params, new ApiListener() {
            @Override
            public void onSuccess(JSONObject object) throws Exception {
                loginApiListener.loginSuccess(object.getString("accessToken"), object.getInt("isAlreadyRegistered") == 1);
            }

            @Override
            public void onSuccess(JSONArray array) throws Exception {
                loginApiListener.loginSuccess(array.getJSONObject(0).getString("accessToken"), array.getJSONObject(0).getInt("isAlreadyRegistered") == 1);
            }

            @Override
            public void onFailure() {
                loginApiListener.onFailure();
            }

            @Override
            public void onFailure(String errorString) {
                loginApiListener.onFailure(errorString);
            }
        });
    }
}
