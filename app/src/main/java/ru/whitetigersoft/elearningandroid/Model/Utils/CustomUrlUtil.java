package ru.whitetigersoft.elearningandroid.Model.Utils;

import android.webkit.URLUtil;

/**
 * Created by Александр on 20.07.2017.
 */

public class CustomUrlUtil {

    public static final String DEFAULT_URL_LAST_NAME_PDF = "default.pdf";

    private static int getLastIndexOfSlash(String url) {
        for (int i = url.length() - 1; (i != 0); i--) {
            if (url.charAt(i) == '/') {
                return i;
            }
        }
        return -1;
    }

    /**
     *
     * @param url - url on file(example: http://orimi.com/pdf-test.pdf)
     * @return last name (example: pdf-test.pdf) or "default.pdf" (if url is not valid).
     * @since 1.0
     */

    public static final String getLastNameOfUrl(String url) {
        if (URLUtil.isValidUrl(url)) {
            String resultUrl = url.trim();
            int lastIndexOfSlash = getLastIndexOfSlash(resultUrl);
            if (lastIndexOfSlash != -1) {
                return url.substring(lastIndexOfSlash + 1, url.length());
            }
        }
        return DEFAULT_URL_LAST_NAME_PDF;
    }
}


