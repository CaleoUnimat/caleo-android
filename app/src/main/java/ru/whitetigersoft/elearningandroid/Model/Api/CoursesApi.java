package ru.whitetigersoft.elearningandroid.Model.Api;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.ApiListener;
import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.CoursesApiListener;
import ru.whitetigersoft.elearningandroid.Model.Models.Course;
import ru.whitetigersoft.elearningandroid.Model.Utils.JsonParser;
import ru.whitetigersoft.elearningandroid.Model.Utils.JsonParserAsync;

/**
 * Created by Александр on 19.07.2017.
 */

public class CoursesApi extends PrivateApi {

    private static final String API_ACTION_LIST = "list";
    private static final String API_ACTION_DETAILS = "details";
    private static final String API_ACTION_START = "start";

    public void startCourse(int courseId, final CoursesApiListener coursesApiListener) {
        String actionApi = API_CONTROLLER_PROJECT + PrivateApi.API_SUB_CONTROLLER_COURSE + API_ACTION_START;
        RequestParams requestParams = new RequestParams();
        requestParams.put("courseId", courseId);
        sendRequest(REQUEST_TYPE_POST, actionApi, requestParams, new ApiListener() {
            @Override
            public void onSuccess(JSONObject object) throws Exception {
                coursesApiListener.onSuccess();
            }

            @Override
            public void onSuccess(JSONArray array) throws Exception {
                coursesApiListener.onSuccess();
            }

            @Override
            public void onFailure() {
                coursesApiListener.onFailure();
            }

            @Override
            public void onFailure(String errorString) {
                coursesApiListener.onFailure(errorString);
            }
        });
    }

    public void getCoursesList(final CoursesApiListener coursesApiListener) {
        String actionApi = API_CONTROLLER_PROJECT + PrivateApi.API_SUB_CONTROLLER_COURSE + API_ACTION_LIST;
        sendRequest(REQUEST_TYPE_GET, actionApi, null, new ApiListener() {
            @Override
            public void onFailure() {
                coursesApiListener.onFailure();
            }

            @Override
            public void onFailure(String errorString) {
                coursesApiListener.onFailure(errorString);
            }

            @Override
            public void onSuccess(JSONObject object) throws Exception {
                JsonParserAsync jsonParserAsync = new JsonParserAsync(Course.class, new JsonParserAsync.JsonAsyncParserListener() {
                    @Override
                    public void done(List<?> objects) {
                        List<Course> courseList = new ArrayList<>();
                        courseList.addAll((Collection<? extends Course>) objects);
                        coursesApiListener.onCoursesListLoaded(courseList);
                    }

                    @Override
                    public void updateProgress(Integer value) {

                    }
                });
                jsonParserAsync.execute(object.getJSONArray("courses"));
            }

            @Override
            public void onSuccess(JSONArray array) throws Exception {

            }
        });
    }

    public void getCourseDetail(int courseId, final CoursesApiListener coursesApiListener) {
        String actionApi = API_CONTROLLER_PROJECT + PrivateApi.API_SUB_CONTROLLER_COURSE + API_ACTION_DETAILS;
        RequestParams params = new RequestParams();
        params.put("courseId", courseId);
        sendRequest(REQUEST_TYPE_GET, actionApi, params, new ApiListener() {
            @Override
            public void onFailure() {
                coursesApiListener.onFailure();
            }

            @Override
            public void onFailure(String errorString) {
                coursesApiListener.onFailure(errorString);
            }

            @Override
            public void onSuccess(JSONObject object) throws Exception {
                Course courseDetail = ((Course) JsonParser.parseEntityFromJson(object.getJSONObject("course"), Course.class));
                coursesApiListener.onCourseDetailLoaded(courseDetail);
            }

            @Override
            public void onSuccess(JSONArray array) throws Exception {

            }

        });
    }
}
