package ru.whitetigersoft.elearningandroid.Model.Api;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import ru.whitetigersoft.elearningandroid.BuildConfig;
import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.ApiListener;
import ru.whitetigersoft.elearningandroid.Model.EventBus.InvalidAccessToken;

public class BaseApi {

    public static final int REQUEST_TYPE_GET = 0;
    public static final int REQUEST_TYPE_POST = 1;
    public static final int APP_JSON_DELAY = 1000;

    public static final String API_CONTROLLER_PROJECT = "project/";
    public static final String API_CONTROLLER_COMMON = "common/";
    public static final String API_CONTROLLER_AUTH = "auth/";

    private ApiListener invalidAccessTokenListener;

    public void sendRequest(int requestType, String actionAPI, RequestParams params, ApiListener listener) {
        if (params == null) {
            params = new RequestParams();
        }
        AsyncHttpClient client = new AsyncHttpClient();
        if (requestType == REQUEST_TYPE_GET) {
            client.get(BuildConfig.API_URL + actionAPI,
                    params,
                    prepareResponseHandler(listener));
        }
        if (requestType == REQUEST_TYPE_POST) {
            client.post(BuildConfig.API_URL + actionAPI,
                    params,
                    prepareResponseHandler(listener));
        }
    }

    public JsonHttpResponseHandler prepareResponseHandler(final ApiListener listener) {
        return new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                JSONObject meta;
                Object data;
                try {
                    meta = response.getJSONObject("meta");
                    if (!meta.getBoolean("success")) {
                        if (meta.has("invalidAccessToken") && meta.getBoolean("invalidAccessToken")) {
                            EventBus.getDefault().post(new InvalidAccessToken());
                        } else {
                            listener.onFailure(meta.getString("error").toString());
                        }
                    } else {
                        data = response.get("data");
                        if (data.getClass().equals(JSONObject.class)) {
                            try {
                                if (((JSONObject) data).length() == 0) {
                                    listener.onSuccess();
                                }
                                listener.onSuccess((JSONObject) data);
                            } catch (Exception e) {
                                e.printStackTrace();
                                listener.onFailure();
                            }
                        } else if (data.getClass().equals(JSONArray.class)) {
                            try {
                                if (((JSONArray) data).length() == 0) {
                                    listener.onSuccess();
                                }
                                listener.onSuccess((JSONArray) data);
                            } catch (Exception e) {
                                e.printStackTrace();
                                listener.onFailure();
                            }
                        } else {
                            listener.onFailure();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listener.onFailure();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listener.onFailure();
            }
        };
    }

}
