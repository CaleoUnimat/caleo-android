    package ru.whitetigersoft.elearningandroid.Model.Helpers;

import android.support.annotation.NonNull;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by valerijsamsonov on 24.08.16.
 */
public class DateHelper {

    public static String getMonthByNumber(@NonNull Integer number) {
        switch (number) {
            case 1:
                return "января";
            case 2:
                return "февраля";
            case 3:
                return "марта";
            case 4:
                return "апреля";
            case 5:
                return "мая";
            case 6:
                return "июня";
            case 7:
                return "июля";
            case 8:
                return "августа";
            case 9:
                return "сентября";
            case 10:
                return "октября";
            case 11:
                return "ноября";
            case 12:
                return "декабря";
        }
        return "error";
    }

    public static String getDayOfWeekFromNumber(@NonNull Integer dayNumber) {
        switch (dayNumber) {
            case 0:
                return "суббота";
            case 1:
                return "воскресенье";
            case 2:
                return "понедельник";
            case 3:
                return "вторник";
            case 4:
                return "среда";
            case 5:
                return "четверг";
            case 6:
                return "пятница";
            default:
                return "нет такого дня";
        }
    }

    public static String getTimeStringFromCalendar(@NonNull Calendar calendar) {

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        StringBuilder builder = new StringBuilder();
        builder.append(((hour > 9) ? hour : "0" + hour));
        builder.append(":");
        builder.append(((minute > 9) ? minute : "0" + minute));
        builder.append(":");
        builder.append(((second > 9) ? second : "0" + second));

        return builder.toString();
    }

    public static String getDateStringFromCalendar(@NonNull Calendar calendar) {

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);

        StringBuilder builder = new StringBuilder();
        builder.append(((day > 9) ? day : "0" + day));
        builder.append(".");
        builder.append(((month > 9) ? month : "0" + month));
        builder.append(".");
        builder.append(year);

        return builder.toString();
    }

    public static String getDateStringFromCalendarWithoutYear(@NonNull Calendar calendar) {

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;

        StringBuilder builder = new StringBuilder();
        builder.append(getDayOfWeekFromNumber(calendar.get(Calendar.DAY_OF_WEEK)));
        builder.append(", ");

        builder.append(((day > 9) ? day : "0" + day));
        builder.append(" ");
        builder.append(getMonthByNumber(month));
        return builder.toString();
    }

    public static String getTimeStringFromDate(Date date) {
        if (date == null) {
            return "дата не известна";
        }
        StringBuilder builder = new StringBuilder();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        builder.append(getTimeStringFromCalendar(calendar));

        return builder.toString();
    }

    public static String getDateAndTimeStringFromDate(Date date) {
        StringBuilder stringBuilder = new StringBuilder();
        Calendar requestedDate = Calendar.getInstance();
        requestedDate.setTime(date);

        Calendar nowDate = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);

        Calendar firstDayInWeek = Calendar.getInstance();
        firstDayInWeek.set(Calendar.DAY_OF_WEEK, nowDate.getFirstDayOfWeek());

        if (requestedDate.get(Calendar.DATE) == nowDate.get(Calendar.DATE)) {
            stringBuilder.append(requestedDate.get(Calendar.HOUR_OF_DAY));
            stringBuilder.append(":");
            if(requestedDate.get(Calendar.MINUTE) < 10) {
                stringBuilder.append("0");
            }
            stringBuilder.append(requestedDate.get(Calendar.MINUTE));
        } else if (requestedDate.get(Calendar.DATE) == yesterday.get(Calendar.DATE)) {
            stringBuilder.append("вчера");
        } else if (requestedDate.get(Calendar.DATE) >= firstDayInWeek.get(Calendar.DATE)) {
            stringBuilder.append(getDayOfWeekFromNumber(requestedDate.get(Calendar.DAY_OF_WEEK)));
        } else {
            stringBuilder.append(getDateStringFromCalendar(requestedDate));
        }
        return stringBuilder.toString();
    }

    public static String getDateAndTimeString(Date date) {
        StringBuilder stringBuilder = new StringBuilder();
        Calendar requestedDate = Calendar.getInstance();
        requestedDate.setTime(date);

        Calendar nowDate = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);

        Calendar firstDayInWeek = Calendar.getInstance();
        firstDayInWeek.set(Calendar.DAY_OF_WEEK, nowDate.getFirstDayOfWeek());

        if (requestedDate.get(Calendar.DATE) == nowDate.get(Calendar.DATE)) {
            stringBuilder.append("сегодня");
        } else if (requestedDate.get(Calendar.DATE) == yesterday.get(Calendar.DATE)) {
            stringBuilder.append("вчера");
        } else if (requestedDate.get(Calendar.DATE) >= firstDayInWeek.get(Calendar.DATE)) {
            stringBuilder.append(getDayOfWeekFromNumber(requestedDate.get(Calendar.DAY_OF_WEEK)));
        } else {
            stringBuilder.append(getDateStringFromCalendarWithoutYear(requestedDate));
        }
        return stringBuilder.toString();
    }

    //    public static String getDateAndTimeStringFromDate(Date date) {
//        if (date == null) {
//            return "дата не известна";
//        }
//        long epoch = date.getTime();
//        return (String) DateUtils.getRelativeTimeSpanString(epoch, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
//    }
    public static String getTimeFromSecond(int second) {
        int hours = second / 3600;
        int remainder = second - hours * 3600;
        int mins = remainder / 60;
        return (hours < 10 ? "0" + hours : hours) + ":" + (mins < 10 ? "0" + mins : mins);
    }

    public static String getDurationTimeFromSecond(int second) {
        return (second / 60) + " : " + (second % 60);
    }
}
