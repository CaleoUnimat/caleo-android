package ru.whitetigersoft.elearningandroid.Model.Api.ApiListener;

/**
 * Created by Александр on 19.07.2017.
 */

public class BaseListener {
    public void onSuccess() {}
    public void onFailure() {}
    public void onFailure(String errorString) {}
}
