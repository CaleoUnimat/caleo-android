package ru.whitetigersoft.elearningandroid.Model.Api;

import android.content.Context;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.ApiListener;
import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.ResultApiListener;
import ru.whitetigersoft.elearningandroid.Model.Models.Result;
import ru.whitetigersoft.elearningandroid.Model.Utils.JsonParser;

/**
 * Created by Александр on 24.07.2017.
 */

public class ResultApi extends PrivateApi {

    private static final String API_ACTION_RESULT = "result";

    public void getResult(int lessonId, final ResultApiListener resultApiListener) {
        RequestParams params = new RequestParams();
        params.put("lessonId", lessonId);
        String actionApi = API_CONTROLLER_PROJECT + API_SUB_CONTROLLER_TEST + API_ACTION_RESULT;
        sendRequest(REQUEST_TYPE_GET, actionApi, params, new ApiListener() {
            @Override
            public void onSuccess(JSONObject object) throws Exception {
                Result result = ((Result) JsonParser.parseEntityFromJson(object, Result.class));
                resultApiListener.onResultLoaded(result);
            }

            @Override
            public void onSuccess(JSONArray array) throws Exception {

            }

            @Override
            public void onFailure() {
                resultApiListener.onFailure();
            }

            @Override
            public void onFailure(String errorString) {
                resultApiListener.onFailure(errorString);
            }
        });
    }
}
