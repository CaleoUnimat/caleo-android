package ru.whitetigersoft.elearningandroid.Model.DataProviders;

import java.util.List;

import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.LessonApiListener;
import ru.whitetigersoft.elearningandroid.Model.App.App;
import ru.whitetigersoft.elearningandroid.Model.Models.Lesson;

/**
 * Created by Александр on 25.07.2017.
 */

public class LessonDataProvider extends BaseDataProvider {

    private int courseId;

    public void setId(int id) {
        this.courseId = id;
    }

    @Override
    protected void loadData(int offset) {
        App.getInstance().getApi().getLessonApi().getLessons(courseId, new LessonApiListener() {
            @Override
            public void onListLessonLoaded(List<Lesson> lessonList) {
                if (getItemList().size() == 0 && lessonList.size() == 0) {
                    onDataEmpty();
                } else {
                    getItemList().clear();
                    onDataLoaded(lessonList);
                }
            }

            @Override
            public void onFailure() {
                onDataFailure();
            }

            @Override
            public void onFailure(String errorString) {
                onDataFailure(errorString);
            }
        });
    }
}
