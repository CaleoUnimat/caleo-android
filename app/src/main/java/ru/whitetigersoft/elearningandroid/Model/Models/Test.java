package ru.whitetigersoft.elearningandroid.Model.Models;

import android.os.Parcel;

import java.util.List;

/**
 * Created by Александр on 21.07.2017.
 */

public class Test extends BaseModel {
    public static final int TYPE_MULTIPLE = 1;
    public static final int TYPE_SINGLE = 0;

    public static final Creator<Test> CREATOR = new Creator<Test>() {
        @Override
        public Test createFromParcel(Parcel in) {
            return new Test(in);
        }

        @Override
        public Test[] newArray(int size) {
            return new Test[size];
        }
    };
    private int testLessonId;
    private List<Question> questions;

    public Test(Parcel in) {

    }
    public Test() {

    }

    public int getTestLessonId() {
        return testLessonId;
    }

    public List<Question> getQuestions() {
        return questions;
    }
}
