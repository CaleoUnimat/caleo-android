package ru.whitetigersoft.elearningandroid.Model.Models;

import java.util.List;

/**
 * Created by Александр on 20.07.2017.
 */

public class CourseDetail extends BaseModel {
    private int courseId;
    private String imageUrl;
    private String title;
    private String courseDescription;
    private int progress;
    private int passedLessonCount;

    public int getCourseId() {
        return courseId;
    }

    public String getImage() {
        return imageUrl;
    }

    public String getName() {
        return title;
    }

    public String getDescription() {
        return courseDescription;
    }

    public int getProgress() {
        return progress;
    }

    public int getPassedLessonCount() {
        return passedLessonCount;
    }


}
