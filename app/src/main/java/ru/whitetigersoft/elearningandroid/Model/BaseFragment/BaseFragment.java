package ru.whitetigersoft.elearningandroid.Model.BaseFragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubePlayer;
import com.thefinestartist.ytpa.enums.Orientation;
import com.thefinestartist.ytpa.utils.YouTubeUrlParser;

import permission.auron.com.marshmallowpermissionhelper.FragmentManagePermission;
import ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.PdfView.PdfViewFragment;
import ru.whitetigersoft.elearningandroid.Activity.MainActivity.Fragments.Test.FragmentTest;
import ru.whitetigersoft.elearningandroid.Activity.YouTube.YoutubePlayerActivity;
import ru.whitetigersoft.elearningandroid.Model.DataProviders.BaseDataProvider;
import ru.whitetigersoft.elearningandroid.Model.Helpers.StringHelper;
import ru.whitetigersoft.elearningandroid.Model.Models.Lesson;
import ru.whitetigersoft.elearningandroid.R;

public class BaseFragment extends FragmentManagePermission {

    public static final String IS_REFRESH_LESSON = "isRefreshLesson";
    public static final String FRAGMENT_ADD_TO_BACKSTACK_KEY = "answerOrg";
    public static final String FRAGMENT_ADD_TO_BACKSTACK_COURSE_DETAIL = "courseDetailAnswerOrg";
    public final static int SNACK_BAR_ERROR_DURATION = 1000000000;
    private static final int YOUTUBE_ACTIVITY_RESULT = 1;
    private static final String BUNDLE_PDF_KEY = "bundlePdfKey";
    private static final String BUNDLE_COURSE_ID_KEY = "bundleCourseIdKey";
    private static final String BUNDLE_RESULT_LESSON = "bundleResultLessonKey";
    private View progressBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (progressBar == null) {
            progressBar = inflater.inflate(R.layout.progress_bar_layout, null);
            progressBar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
            container.addView(progressBar);
        }
        return progressBar;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    protected void startTestFragment(Lesson lesson, int courseId) {
        FragmentTest fragmentTest = new FragmentTest();
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_COURSE_ID_KEY, courseId);
        bundle.putParcelable(BUNDLE_RESULT_LESSON, lesson);
        fragmentTest.setArguments(bundle);
        replaceLessonFragment(fragmentTest);
    }

    protected void startPdfView(Lesson lesson) {
        PdfViewFragment pdfViewFragment = new PdfViewFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_PDF_KEY, lesson);
        pdfViewFragment.setArguments(bundle);
        replaceLessonFragment(pdfViewFragment);
    }

    protected void startYouTube(Lesson lesson, int courseId) {
        Intent intent = new Intent(getActivity(), YoutubePlayerActivity.class);
        if (!StringHelper.isEmpty(lesson.getYouTubeUrl())) {
            String videoId = YouTubeUrlParser.getVideoId(lesson.getYouTubeUrl());
            if (videoId != null) {
                intent.putExtra(YoutubePlayerActivity.EXTRA_VIDEO_ID, videoId)
                        .putExtra(YoutubePlayerActivity.EXTRA_LESSON, lesson)
                        .putExtra(YoutubePlayerActivity.EXTRA_COURSE_ID, courseId)
                        .putExtra(YoutubePlayerActivity.EXTRA_PLAYER_STYLE, YouTubePlayer.PlayerStyle.DEFAULT)
                        .putExtra(YoutubePlayerActivity.EXTRA_ORIENTATION, Orientation.AUTO)
                        .putExtra(YoutubePlayerActivity.EXTRA_SHOW_AUDIO_UI, true)
                        .putExtra(YoutubePlayerActivity.EXTRA_HANDLE_ERROR, true)
                        .putExtra(YoutubePlayerActivity.EXTRA_SEEK_TO, 0);
                startActivityForResult(intent, YOUTUBE_ACTIVITY_RESULT);
            } else {
                if (getView() != null) {
                    Snackbar.make(getView(), getString(R.string.snack_bar_message_error_youtube), Snackbar.LENGTH_LONG).show();
                    exitFromLesson();
                }
            }
        } else {
            if (getView() != null) {
                Snackbar.make(getView(), getString(R.string.snack_bar_message_error_youtube), Snackbar.LENGTH_LONG).show();
                exitFromLesson();
            }
        }
    }

    protected void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected void replaceLessonFragment(BaseFragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.replace(R.id.container_lesson, fragment);
        transaction.commit();
    }

    public void setTitle(String title) {
        getActivity().setTitle(title);
    }

    public void showProgressBarWithLongerDuration(String text) {
        if (progressBar != null) {
            progressBar.bringToFront();
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    public Snackbar getSnackbar(View rootView, String message, int duration) {
        Snackbar snackbar = Snackbar.make(rootView, message, duration);
        TextView textView = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setSingleLine(false);
        return snackbar;
    }

    public void showSnackBarError(View view, String text, final BaseDataProvider baseDataProvider) {
        if (getContext() != null) {
            Snackbar snackbar = getSnackbar(view, text, SNACK_BAR_ERROR_DURATION)
                    .setAction(getString(R.string.snackbar_button_reload), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (getContext() != null) {
                                showProgressBarWithLongerDuration(getString(R.string.snackbar_button_reload));
                                baseDataProvider.reloadData();
                            }
                        }
                    });
            snackbar.show();
        }
    }

    public void hideProgressBar() {
        if (progressBar != null) {
            progressBar.animate()
                    .alpha(0.0f)
                    .setDuration(10)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        }
    }

    protected void replaceFragment(BaseFragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(FRAGMENT_ADD_TO_BACKSTACK_KEY);
        transaction.replace(R.id.container, fragment);
        transaction.commit();
    }

    @Override
    public void onDetach() {
        hideProgressBar();
        super.onDetach();
    }

    protected void exitFromLesson() {
        Intent intent = new Intent();
        intent.putExtra(IS_REFRESH_LESSON, true);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    @Override
    public void onPause() {
        super.onPause();
        hideProgressBar();
        final InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && inputMethodManager.isActive()) {
            if (getActivity().getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        }
    }
}