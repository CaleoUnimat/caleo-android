package ru.whitetigersoft.elearningandroid.Model.Utils;


import android.util.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JsonParser {

    private static GsonBuilder builder = null;

    public static List<?> parseEntitiesFromJson(JSONArray array, Class c) {
        List<Object> list = new ArrayList<>();
        GsonBuilder builder = getBuilder();
        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject object = array.getJSONObject(i);
                Object convertedObject = convert(object, c, builder);
                list.add(convertedObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    private static Object convert(JSONObject object, Class c) {
        GsonBuilder builder = getBuilder();
        return convert(object, c, builder);
    }

    private static Object convert(JSONObject object, Class c, GsonBuilder builder) {
        Gson gson = builder.create();
        Object obj = null;
        try {
            obj = gson.fromJson(object.toString(), c);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static GsonBuilder getBuilder() {
        builder = new GsonBuilder();

        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                long timestamp = json.getAsJsonPrimitive().getAsInt();
                Date date = new Date(timestamp * 1000);
                return date;
            }
        });

        builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
            @Override
            public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
                return new JsonPrimitive(src.getTime() / 1000);
            }
        });

        builder.registerTypeAdapter(Boolean.class, new JsonDeserializer<Boolean>() {
            @Override
            public Boolean deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                int val = json.getAsJsonPrimitive().getAsInt();
                return val == 1;
            }
        });


        Type type = new TypeToken<List<Pair<Integer, String>>>() {
        }.getType();
        builder.registerTypeAdapter(type, new JsonDeserializer<List<Pair<Integer, String>>>() {
            @Override
            public List<Pair<Integer, String>> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                List<Pair<Integer, String>> list = new ArrayList<Pair<Integer, String>>();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(json.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONArray names = null;
                try {
                    names = jsonObject.names();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                for (int i = 0; i < names.length(); i++) {
                    try {
                        list.add(new Pair<Integer, String>(Integer.parseInt(names.getString(i)), jsonObject.getString(names.getString(i))));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                return list;
            }
        });
        return builder;
    }

    public static Object parseEntityFromJson(JSONObject object, Class c) {
        Object obj = convert(object, c);
        return obj;
    }
}
