package ru.whitetigersoft.elearningandroid.Model.Helpers;

import java.util.List;

/**
 * Created by valerijsamsonov on 29.05.17.
 */

public class ListHelper {
    public static boolean isEmpty(List<?> objects) {
        if (objects == null) {
            return true;
        }
        if (objects.size() == 0) {
            return true;
        }
        return false;
    }
}
