package ru.whitetigersoft.elearningandroid.Model.Api.ApiListener;

import java.util.List;

import ru.whitetigersoft.elearningandroid.Model.Models.Lesson;

/**
 * Created by Александр on 24.07.2017.
 */

public class LessonApiListener extends BaseListener {
    public void onListLessonLoaded(List<Lesson> lessonList) {}
}
