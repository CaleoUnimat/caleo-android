package ru.whitetigersoft.elearningandroid.Model.Api.ApiListener;

import java.util.List;

import ru.whitetigersoft.elearningandroid.Model.Models.Course;
import ru.whitetigersoft.elearningandroid.Model.Models.CourseDetail;

/**
 * Created by Александр on 19.07.2017.
 */

public class CoursesApiListener extends BaseListener {
    public void onCoursesListLoaded(List<Course> courseList) {}
    public void onCourseDetailLoaded(Course courseDetail) {}
}
