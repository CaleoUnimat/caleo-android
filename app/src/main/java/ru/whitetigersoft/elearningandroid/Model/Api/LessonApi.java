package ru.whitetigersoft.elearningandroid.Model.Api;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.ApiListener;
import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.LessonApiListener;
import ru.whitetigersoft.elearningandroid.Model.Models.Lesson;
import ru.whitetigersoft.elearningandroid.Model.Utils.JsonParserAsync;

/**
 * Created by Александр on 24.07.2017.
 */

public class LessonApi extends PrivateApi {

    private static final String API_ACTION_LIST = "list";
    private static final String API_ACTION_PASS_VIDEO = "pass-video-lesson";
    private static final String API_ACTION_PASS_TEXT = "pass-text-lesson";

    public void passVideoLesson(int lessonId, int progress, final LessonApiListener lessonApiListener) {
        RequestParams params = new RequestParams();
        params.put("lessonId", lessonId);
        params.put("progress", progress);
        String actionApi = API_CONTROLLER_PROJECT + API_SUB_CONTROLLER_LESSON + API_ACTION_PASS_VIDEO;
        sendRequest(REQUEST_TYPE_POST, actionApi, params, new ApiListener() {
            @Override
            public void onSuccess(JSONObject object) throws Exception {
                lessonApiListener.onSuccess();
            }

            @Override
            public void onSuccess(JSONArray array) throws Exception {

            }

            @Override
            public void onFailure() {
                lessonApiListener.onFailure();
            }

            @Override
            public void onFailure(String errorString) {
                lessonApiListener.onFailure(errorString);
            }
        });
    }

    public void passTextLesson(int lessonId, final LessonApiListener lessonApiListener) {
        RequestParams params = new RequestParams();
        params.put("lessonId", lessonId);
        String actionApi = API_CONTROLLER_PROJECT + API_SUB_CONTROLLER_LESSON + API_ACTION_PASS_TEXT;
        sendRequest(REQUEST_TYPE_POST, actionApi, params, new ApiListener() {
            @Override
            public void onSuccess(JSONObject object) throws Exception {
                lessonApiListener.onSuccess();
            }

            @Override
            public void onSuccess(JSONArray array) throws Exception {

            }

            @Override
            public void onFailure() {
                lessonApiListener.onFailure();
            }

            @Override
            public void onFailure(String errorString) {
                lessonApiListener.onFailure(errorString);
            }
        });
    }

    public void getLessons(int courseId, final LessonApiListener lessonApiListener) {
        RequestParams params = new RequestParams();
        params.put("courseId", courseId);
        String actionApi = API_CONTROLLER_PROJECT + API_SUB_CONTROLLER_LESSON + API_ACTION_LIST;
        sendRequest(REQUEST_TYPE_GET, actionApi, params, new ApiListener() {
            @Override
            public void onSuccess(JSONObject object) throws Exception {
                JsonParserAsync jsonParserAsync = new JsonParserAsync(Lesson.class, new JsonParserAsync.JsonAsyncParserListener() {
                    @Override
                    public void done(List<?> objects) {
                        List<Lesson> lessonList = new ArrayList<>();
                        lessonList.addAll((Collection<? extends Lesson>) objects);
                        lessonApiListener.onListLessonLoaded(lessonList);
                    }

                    @Override
                    public void updateProgress(Integer value) {
                    }
                });
                jsonParserAsync.execute(object.getJSONArray("lessons"));
            }

            @Override
            public void onSuccess(JSONArray array) throws Exception {
                JsonParserAsync jsonParserAsync = new JsonParserAsync(Lesson.class, new JsonParserAsync.JsonAsyncParserListener() {
                    @Override
                    public void done(List<?> objects) {
                        List<Lesson> lessonList = new ArrayList<>();
                        lessonList.addAll((Collection<? extends Lesson>) objects);
                        lessonApiListener.onListLessonLoaded(lessonList);
                    }

                    @Override
                    public void updateProgress(Integer value) {

                    }
                });
                jsonParserAsync.execute(array);
            }

            @Override
            public void onFailure() {
                lessonApiListener.onFailure();
            }

            @Override
            public void onFailure(String errorString) {
                lessonApiListener.onFailure(errorString);
            }
        });
    }
}
