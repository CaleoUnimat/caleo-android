package ru.whitetigersoft.elearningandroid.Model.Models;

import android.os.Parcel;

/**
 * Created by Александр on 20.07.2017.
 */

public class Lesson extends BaseModel {
    public static final int TYPE_TEXT = 1;
    public static final int TYPE_VIDEO = 2;
    public static final int TYPE_TEST = 3;

    public static final Creator<Lesson> CREATOR = new Creator<Lesson>() {
        @Override
        public Lesson createFromParcel(Parcel in) {
            return new Lesson(in);
        }

        @Override
        public Lesson[] newArray(int size) {
            return new Lesson[size];
        }
    };

    private int lessonId;
    private String title;
    private int type;
    private float progress;
    private Boolean isCompleted;
    private String youTubeUrl;
    private String pdfUrl;
    private int testLessonId;

    public Lesson() {
        isCompleted = false;
        progress = 0;
    }

    public Lesson(Parcel in) {
        lessonId = in.readInt();
        title = in.readString();
        type = in.readInt();
        progress = in.readFloat();
        isCompleted = in.readByte() != 0;
        youTubeUrl = in.readString();
        pdfUrl = in.readString();
        testLessonId = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(lessonId);
        dest.writeString(title);
        dest.writeInt(type);
        dest.writeFloat(progress);
        dest.writeByte((byte) (isCompleted ? 1 : 0));
        dest.writeString(youTubeUrl);
        dest.writeString(pdfUrl);
        dest.writeInt(testLessonId);
    }

    public int getLessonId() {
        return lessonId;
    }

    public String getTitle() {
        return title;
    }

    public int getType() {
        return type;
    }

    public String getYouTubeUrl() {
        return youTubeUrl;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public float getProgress() {
        return progress;
    }

    public Boolean isCompleted() {
        return isCompleted;
    }

    public int getTestLessonId() {
        return testLessonId;
    }
}
