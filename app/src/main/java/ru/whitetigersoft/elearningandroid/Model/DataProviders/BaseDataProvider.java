package ru.whitetigersoft.elearningandroid.Model.DataProviders;

import java.util.ArrayList;
import java.util.List;

import ru.whitetigersoft.elearningandroid.Model.DataProviders.DataProviderListener.BaseDataProviderListener;

/**
 * Created by Александр on 25.07.2017.
 */

public class    BaseDataProvider {
    private BaseDataProviderListener providerListener;
    private boolean isLoading, isAllLoaded;
    private List<Object> itemList;

    public BaseDataProvider() {
        this.itemList = new ArrayList<>();
    }

    public void setBaseDataProvider(BaseDataProviderListener baseDataProvider) {
        this.providerListener = baseDataProvider;
    }

    public void reloadData() {
        isAllLoaded = false;
        isLoading = false;
        itemList = new ArrayList<>();
        loadNextDataPart();
    }

    public void loadNextDataPart() {
        if (isLoading || isAllLoaded) {
            return;
        }
        isLoading = true;
        loadData(itemList.size());
    }

    protected void onDataLoaded(List<?> items) {
        isLoading = false;
        if (items != null) {
            if (items.size() > 0) {
                itemList.addAll(items);
                providerListener.onItemsLoaded(itemList);
            } else {
                providerListener.onAllItemsLoaded();
                isAllLoaded = true;
            }
        }
    }

    protected void onDataFailure() {
        isLoading = false;
        providerListener.onError();
    }

    protected void onDataEmpty() {
        isLoading = false;
        providerListener.onEmpty();
    }

    protected void onDataFailure(String errorString) {
        isLoading = false;
        providerListener.onError(errorString);
    }

    protected void loadData(int offset) {}

    protected List<Object> getItemList() {
        return itemList;
    }

}
