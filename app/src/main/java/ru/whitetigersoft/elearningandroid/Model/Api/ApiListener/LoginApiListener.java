package ru.whitetigersoft.elearningandroid.Model.Api.ApiListener;

/**
 * Created by Александр on 19.07.2017.
 */

abstract public class LoginApiListener extends BaseListener {
    public abstract void loginSuccess(String accessToken, boolean isAlreadyRegistered);
}
