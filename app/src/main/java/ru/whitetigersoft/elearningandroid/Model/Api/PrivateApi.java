package ru.whitetigersoft.elearningandroid.Model.Api;

import com.loopj.android.http.RequestParams;

import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.ApiListener;
import ru.whitetigersoft.elearningandroid.Model.App.App;

public class PrivateApi extends BaseApi {

    protected static final String API_SUB_CONTROLLER_COURSE = "course/";
    protected static final String API_SUB_CONTROLLER_LESSON = "lesson/";
    protected static final String API_SUB_CONTROLLER_TEST = "test/";

    @Override
    public void sendRequest(int requestType, String apiAction, RequestParams params, ApiListener listener) {
        if (params == null) {
            params = new RequestParams();
        }
        params.put("accessToken", App.getInstance().getAppUser().getAccessToken());
        super.sendRequest(requestType, apiAction, params, listener);
    }
}
