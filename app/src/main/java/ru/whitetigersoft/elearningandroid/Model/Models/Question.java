package ru.whitetigersoft.elearningandroid.Model.Models;

import java.util.List;

/**
 * Created by Александр on 21.07.2017.
 */

public class Question extends BaseModel {
    private int testQuestionId;
    private int type;
    private String question;
    private String answerString;
    private List<Answers> answers;

    public int getTestQuestionId() {
        return testQuestionId;
    }

    public String getAnswerString() {
        return answerString;
    }

    public String getQuestion() {
        return question;
    }

    public int getType() {
        return type;
    }

    public List<Answers> getAnswers() {
        return answers;
    }
}
