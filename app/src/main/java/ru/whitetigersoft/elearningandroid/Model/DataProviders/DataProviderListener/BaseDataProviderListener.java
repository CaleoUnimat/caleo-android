package ru.whitetigersoft.elearningandroid.Model.DataProviders.DataProviderListener;

import java.util.List;

/**
 * Created by Александр on 25.07.2017.
 */

public class BaseDataProviderListener {
    public void onAllItemsLoaded() {}
    public void onItemsLoaded(List<?> items) {}
    public void onError(String errorString) {}
    public void onError() {}
    public void onEmpty() {}
}
