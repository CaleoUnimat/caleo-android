package ru.whitetigersoft.elearningandroid.Model.App.AppUser;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Александр on 02.08.2017.
 */
public class AppUser {

    private static final String SAVED_USER_ACCESS_TOKEN = "userAccessToken";

    private String accessToken;
    private SharedPreferences sharedPreferences;

    public AppUser(Context context) {
        sharedPreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
    }

    public String getAccessToken() {
        accessToken = sharedPreferences.getString(SAVED_USER_ACCESS_TOKEN, "");
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SAVED_USER_ACCESS_TOKEN, this.accessToken);
        editor.apply();
    }

    public void logOut() {
        accessToken = null;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public boolean isAuthenticated() {
        return ((getAccessToken() != null) && (!getAccessToken().isEmpty()));
    }

}
