package ru.whitetigersoft.elearningandroid.Model.CustomListeners;

/**
 * Created by Александр on 03.08.2017.
 */

abstract public class OnEnableRecycleViewListener {
    public abstract void onEnable();
    public abstract void onDisable(int position);
    public abstract void onFailure();
    public abstract void onFailure(String errorString);
}
