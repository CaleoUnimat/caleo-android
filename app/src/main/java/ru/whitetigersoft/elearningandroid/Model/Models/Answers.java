package ru.whitetigersoft.elearningandroid.Model.Models;

/**
 * Created by Александр on 07.08.2017.
 */

public class Answers extends BaseModel {
    private int testQuestionAnswerId;
    private String answer;
    private Boolean isRightAnswer;

    public int getTestQuestionAnswerId() {
        return testQuestionAnswerId;
    }

    public String getAnswer() {
        return answer;
    }

    public Boolean isRightAnswer() {
        return isRightAnswer;
    }

    public void setTestQuestionAnswerId(int testQuestionAnswerId) {
        this.testQuestionAnswerId = testQuestionAnswerId;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setRightAnswer(Boolean rightAnswer) {
        isRightAnswer = rightAnswer;
    }
}
