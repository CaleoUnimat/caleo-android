package ru.whitetigersoft.elearningandroid.Model.Api.ApiListener;

import org.json.JSONArray;
import org.json.JSONObject;


public abstract class ApiListener extends BaseListener {
    public abstract void onSuccess(JSONObject object) throws Exception;
    public abstract void onSuccess(JSONArray array) throws Exception;
}
