package ru.whitetigersoft.elearningandroid.Model.DataProviders;

import java.util.List;

import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.CoursesApiListener;
import ru.whitetigersoft.elearningandroid.Model.App.App;
import ru.whitetigersoft.elearningandroid.Model.Models.Course;

/**
 * Created by Александр on 25.07.2017.
 */

public class CourseDataProvider extends BaseDataProvider {
    @Override
    protected void loadData(int offset) {
        App.getInstance().getApi().getCoursesApi().getCoursesList(new CoursesApiListener() {
            @Override
            public void onCoursesListLoaded(List<Course> courseList) {
                onDataLoaded(courseList);
            }

            @Override
            public void onFailure() {
                onDataFailure();
            }

            @Override
            public void onFailure(String errorString) {
                onDataFailure(errorString);
            }
        });
    }
}
