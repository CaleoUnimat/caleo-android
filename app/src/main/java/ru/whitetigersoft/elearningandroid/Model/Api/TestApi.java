package ru.whitetigersoft.elearningandroid.Model.Api;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.ApiListener;
import ru.whitetigersoft.elearningandroid.Model.Api.ApiListener.TestApiListener;
import ru.whitetigersoft.elearningandroid.Model.Models.Test;
import ru.whitetigersoft.elearningandroid.Model.Utils.JsonParser;

/**
 * Created by Александр on 21.07.2017.
 */

public class TestApi extends PrivateApi {

    private static final String API_ACTION_PASS_TEST = "pass-test";
    private static final String API_ACTION_TEST_DETAIL = "details";
    private static final String API_ACTION_TEST_CHECK = "check-passing";

    public void passTest(int lessonId, String answers, final TestApiListener testApiListener) {
        String actionApi = API_CONTROLLER_PROJECT + API_SUB_CONTROLLER_LESSON + API_ACTION_PASS_TEST;
        RequestParams params = new RequestParams();
        params.put("lessonId", lessonId);
        if (answers != null) {
            params.put("answers", answers);
        }
        sendRequest(REQUEST_TYPE_POST, actionApi, params, new ApiListener() {
            @Override
            public void onSuccess(JSONObject object) throws Exception {
                testApiListener.onSuccess();
            }

            @Override
            public void onSuccess(JSONArray array) throws Exception {

            }

            @Override
            public void onFailure() {
                testApiListener.onFailure();
            }

            @Override
            public void onFailure(String errorString) {
                testApiListener.onFailure(errorString);
            }
        });
    }

    public void getTests(int lessonId, final TestApiListener testApiListener) {
        String actionApi = API_CONTROLLER_PROJECT + API_SUB_CONTROLLER_TEST + API_ACTION_TEST_DETAIL;
        RequestParams params = new RequestParams();
        params.put("lessonId", lessonId);
        sendRequest(REQUEST_TYPE_GET, actionApi, params, new ApiListener() {
            @Override
            public void onSuccess(JSONObject object) throws Exception {
                Test test = ((Test) JsonParser.parseEntityFromJson(object.getJSONObject("test"), Test.class));
                testApiListener.onTestLoaded(test);
            }

            @Override
            public void onSuccess(JSONArray array) throws Exception {

            }

            @Override
            public void onFailure() {
                testApiListener.onFailure();
            }

            @Override
            public void onFailure(String errorString) {
                testApiListener.onFailure(errorString);
            }
        });
    }

    public void checkPassingTest(int testLessonId, final TestApiListener testApiListener) {
        String actionApi = API_CONTROLLER_PROJECT + API_SUB_CONTROLLER_TEST + API_ACTION_TEST_CHECK;
        RequestParams params = new RequestParams();
        params.put("testLessonId", testLessonId);
        sendRequest(REQUEST_TYPE_GET, actionApi, params, new ApiListener() {
            @Override
            public void onSuccess(JSONObject object) throws Exception {
                Integer timeInSeconds = object.getInt("remainingTime");
                testApiListener.onCheckTestPassing(timeInSeconds);
            }

            @Override
            public void onSuccess(JSONArray array) throws Exception {

            }

            @Override
            public void onFailure() {
                testApiListener.onFailure();
            }

            @Override
            public void onFailure(String errorString) {
                testApiListener.onFailure(errorString);
            }
        });
    }
}
