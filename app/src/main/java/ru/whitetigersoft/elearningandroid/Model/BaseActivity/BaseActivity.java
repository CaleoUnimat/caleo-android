package ru.whitetigersoft.elearningandroid.Model.BaseActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import ru.whitetigersoft.elearningandroid.Activity.Login.LoginActivity;
import ru.whitetigersoft.elearningandroid.Activity.Splash.SplashActivity;
import ru.whitetigersoft.elearningandroid.Model.App.App;
import ru.whitetigersoft.elearningandroid.Model.EventBus.InvalidAccessToken;
import ru.whitetigersoft.elearningandroid.R;
import ru.whitetigersoft.elearningandroid.View.CustomTextView;

public class BaseActivity extends AppCompatActivity {

    private View viewProgressBar;
    private Toolbar toolbar;
    private View mainView;
    private FragmentManager fragmentManager;
    private CustomTextView titleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getSupportFragmentManager();
        if (App.getInstance() != null) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            setCustomTitle();
            prepareViews();
            setupDrawer();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.toast_message_error_on_load), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Subscribe
    public void onInvalidToken(InvalidAccessToken event) {
        App.getInstance().getAppUser().logOut();
        startActivity(new Intent(this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onBackPressed() {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        }
    }

    private void setCustomTitle() {
        titleView = new CustomTextView(this);
        Typeface face = Typeface.createFromAsset(this.getAssets(), "fonts/MagistralC-Bold.otf");
        titleView.setTypeface(face);
        titleView.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        toolbar.addView(titleView);
    }

    protected void setupDrawer() {
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (fragmentManager.getBackStackEntryCount() > 0) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_arrow_back_white_24dp));
                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                }
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragmentManager.getBackStackEntryCount() > 0) {
                    fragmentManager.popBackStack();
                }
            }
        });
    }

    protected void setAppBarVisibility(int visibility) {
        toolbar.setVisibility(visibility);
    }

    protected View getMainView() {
        return mainView;
    }

    private void prepareViews() {
        mainView = findViewById(R.id.content_main);
        toolbar.setTitleTextColor(ContextCompat.getColor(getApplicationContext(), android.R.color.white));
    }

    @Override
    public void setContentView(int layoutResId) {
        super.setContentView(layoutResId);
        viewProgressBar = getLayoutInflater().inflate(R.layout.progress_bar_layout, null);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER;
        this.addContentView(viewProgressBar, params);
    }

    public void showProgressBar(String text) {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        viewProgressBar.bringToFront();
        viewProgressBar.startAnimation(animation);
        viewProgressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        viewProgressBar.startAnimation(animation);
        viewProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void setTitle(CharSequence title) {
        try {
            super.setTitle("");
            titleView.setText(title);
        } catch (Exception e) {
            super.setTitle(title);
        }
    }
}