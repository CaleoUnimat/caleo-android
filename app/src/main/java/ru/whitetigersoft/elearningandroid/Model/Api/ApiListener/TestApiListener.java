package ru.whitetigersoft.elearningandroid.Model.Api.ApiListener;

import ru.whitetigersoft.elearningandroid.Model.Models.Test;

/**
 * Created by Александр on 21.07.2017.
 */

abstract public class TestApiListener extends BaseListener {
    public void onTestLoaded(Test test){}
    public void onCheckTestPassing(Integer remainingTime){}
}
